# softsdl2

Python extension module that provides CPU-only functions on SDL2 surfaces and windows.

## Getting started

To build the extension module do:

    python3 setup.py build_ext --inplace

## API design

The API might appear somewhat unusual in that all functions are operate in "batch". For
example, there is a draw_pixels function that takes a list of coordinates as arguments and
draw a pixel on each of those coordinates. There are analogous functions for drawing lines,
rectangles, etc. The reason for this design is to reduce the overhead of calling C functions
from Python.

## Acknowledgement

This code was mostly written by ChatGPT 4.