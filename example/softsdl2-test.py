import softsdl2
import numpy as np
import random
import time

print(softsdl2.screen_info())

#win_handle0 = softsdl2.window_create("My SDL Window", 800, 600)
win_handle = softsdl2.window_create("My SDL Window", 800, 600)
surface = softsdl2.window_surface(win_handle)

fresh_surface = softsdl2.surface_new(200, 300)
softsdl2.draw_clear(fresh_surface, (255, 64, 32, 255))

#surface = softsdl2.surface_subsurface(surface, 20, 20, 600, 500)

pixels = np.array([[100, 100], [110, 110], [120, 120]], dtype=np.int32)  # List of (x, y) coordinates
lines = np.array([[10, 10, 20, 20], [200, 200, 300, 300], [400, 400, 20000, 20000]], dtype=np.int32)  # List of (x, y) coordinates

polyline = np.array([[10, 500], [20, 450], [30, 470], [50, 300], [60, 520]], dtype=np.int32)  # List of (x, y) coordinates

triangles = np.array([[100, 100, 200, 200, 300, 100, 1], [400, 420, 500, 540, 400, 350, 0]], dtype=np.int32)
triangles_f_3d = np.array([[100, 100, 0, 200, 200, 0, 300, 100, 0], [150, 120, -0.1, 250, 240, -0.1, 120, 170, -0.1]], dtype=np.float32)
colors = np.array([[255, 0, 0, 128], [0, 255, 255, 255]], dtype=np.uint8)

boxes = np.array([[10, 10, 50, 50], [100, 50, 40, 40], [200, 100, 30, 60]], dtype=np.int32)

circles = np.array([
    (100, 100, 50, 0),   # Filled circle at (100, 100) with radius 50
    (300, 100, 50, 5),   # Circle with border width 5 at (300, 100) with radius 50
    (100, 300, 75, 0),   # Filled circle at (100, 300) with radius 75
    (300, 300, 75, 10)   # Circle with border width 10 at (300, 300) with radius 75
], dtype=np.int32)

rects = np.zeros((100, 5), dtype=np.int32)
for i in range(0, 100):
    rects[i] = [random.randint(0, 300), random.randint(0, 300), random.randint(10, 100), random.randint(0, 100), random.randint(0, 5)]

font = softsdl2.font_load("/usr/share/fonts/truetype/abyssinica/AbyssinicaSIL-Regular.ttf", 20)
hello_world = softsdl2.font_render(font, "Hello, world!", (192, 168, 0))



frames = 10000
begin = time.time()
hx = 100
hy = 214
dx = 1
dy = 1
done = False

width, height = softsdl2.surface_size(surface)
    
idxbuffer = np.full((height, width), -1, dtype=np.int32)
zbuffer = np.full((height, width), np.inf, dtype=np.float32)


draw_list0 = [("line", 400, 400, 600, 600, (255, 255, 255)),
             ("rectangle", 500, 500, 100, 100, 0, (0, 0, 255, 32)),
             ("rectangle", 550, 550, 25, 25, 5, (255, 0, 255, 128)),
             ("triangle", 600, 400, 650, 500, 625, 550, 1, (0, 255, 255, 128)),
             ("triangle", 400, 500, 550, 650, 525, 650, 0, (255, 0, 192, 128))]

draw_list = [("draw", draw_list0, (-100, -100), 128),
             ("draw", draw_list0, (100, 100), 255),
             ("draw", draw_list0, (-500, -500), 255, (500, 400, 200, 200))]

text_scale = 1.0
d_text_scale = 0.1
draw_alpha = 0
d_draw_alpha = 1
for _ in range(0, frames):
    events = softsdl2.events_get(600)
    surface = softsdl2.window_surface(win_handle)
    for ev in events:
        match ev:
            case {'type': 'WINDOWEVENT', 'event': 'WINDOWEVENT_CLOSE', 'window': window}:
                done = True
            case {'type': 'WINDOWEVENT', 'event': 'WINDOWEVENT_MAXIMIZED', 'window': window}:
                print("MAXIMIZED!", softsdl2.surface_size(surface))
    if done:
        break
    color = (0, 0, 255, 192)
    softsdl2.draw_clear(surface, (32, 64, 32, 255))

    color = (255, 255, 255, 64)  # RGBA

    softsdl2.draw_rectangles(surface, rects, color)
    softsdl2.draw_polyline(surface, polyline, color)

    softsdl2.draw_pixels(surface, pixels, color)
    softsdl2.surface_blit(surface, hello_world, (hx, hy), text_scale, alpha=draw_alpha)
    softsdl2.surface_blit(surface, fresh_surface, (100, 200))
    softsdl2.draw_triangles(surface, triangles, colors)
    softsdl2.draw_circles(surface, circles, (255, 32, 102))
    
    zbuffer[:,:] = np.inf

    softsdl2.draw_triangles_w_zbuffer(surface, triangles_f_3d, colors, zbuffer, idxbuffer)

    softsdl2.draw(surface, draw_list, origin=(hy - 400, hx - 400), alpha=draw_alpha)
    #softsdl2.draw(surface, draw_list, origin=(0, 0), alpha=draw_alpha)
    
    draw_alpha += d_draw_alpha
    if draw_alpha == 255 or draw_alpha == 0:
        d_draw_alpha = -d_draw_alpha

    hx += dx
    hy += dy
    if hx == 600 or hx == 0:
        dx = -dx
    if hy == 500 or hy == 0:
        dy = -dy
    triangles_f_3d[0] += 0.06
    triangles_f_3d[1] += 0.2

    if text_scale > 50 or text_scale < 0.1:
        d_text_scale = -d_text_scale
    text_scale += d_text_scale

    softsdl2.window_update(win_handle)
    #print(softsdl2.window_size(win_handle))
print(frames/ (time.time() - begin))

softsdl2.window_close(win_handle)
softsdl2.quit()
