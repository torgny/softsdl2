from setuptools import setup, Extension
import numpy as np

import os

# Use pkg-config to get flags
def pkgconfig(*packages, **kw):
      flag_map = {'-I': 'include_dirs', '-L': 'library_dirs', '-l': 'libraries'}
      for token in os.popen("pkg-config --libs --cflags %s" % ' '.join(packages)).read().split():
            if token[:2] in flag_map:
                  kw.setdefault(flag_map.get(token[:2]), []).append(token[2:])
      return kw

module = Extension('softsdl2',
                   sources = ['src/softsdl2.c'],
                   extra_compile_args=['-O3'],
                   **pkgconfig('sdl2', 'SDL2_ttf', 'SDL2_image', include_dirs=[np.get_include()]))

setup(name = 'softsdl2',
      version = '1.0',
      description = 'Python module for creating SDL windows',
      ext_modules = [module])
