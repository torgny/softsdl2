#define PY_SSIZE_T_CLEAN
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>
#include <SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>

static int ensure_sdl_initialized() {
    static int sdl_initialized = 0;

    if (!sdl_initialized) {
        if (SDL_Init(SDL_INIT_VIDEO) < 0) {
            PyErr_Format(PyExc_RuntimeError, "SDL_GetCurrentDisplayMode Error: %s", SDL_GetError());
            return 0;
        }
        sdl_initialized = 1;
    }

    return 1;
}

static PyObject* screen_info(PyObject* self, PyObject* args) {
    if (!ensure_sdl_initialized()) {
        return NULL; // Error is already set in ensure_sdl_initialized
    }

    SDL_DisplayMode displayMode;
    if (SDL_GetCurrentDisplayMode(0, &displayMode) != 0) {
        return PyErr_Format(PyExc_RuntimeError, "SDL_GetCurrentDisplayMode Error: %s", SDL_GetError());
    }

    float ddpi, hdpi, vdpi;
    if (SDL_GetDisplayDPI(0, &ddpi, &hdpi, &vdpi) != 0) {
        return PyErr_Format(PyExc_RuntimeError, "SDL_GetDisplayDPI Error: %s", SDL_GetError());
    }

    int width = displayMode.w;
    int height = displayMode.h;
    int bpp = SDL_BITSPERPIXEL(displayMode.format);

    return Py_BuildValue("(iiifff)", width, height, bpp, ddpi, hdpi, vdpi);
}

static PyObject* window_create(PyObject* self, PyObject* args, PyObject* keywds) {
    const char* title;
    int width;
    int height;
    int show = 1; // Default value is true

    static char *kwlist[] = {"title", "width", "height", "show", NULL};

    if (!PyArg_ParseTupleAndKeywords(args, keywds, "sii|p", kwlist, &title, &width, &height, &show))
        return NULL;

    if (!ensure_sdl_initialized()) {
        return NULL; // Error set inisze ensure_sdl_initialized
    }

    Uint32 flags = SDL_WINDOW_RESIZABLE;
    if (show) {
        flags |= SDL_WINDOW_SHOWN;
    }

    SDL_Window* window = SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, flags);
    if (window == NULL) {
        return PyErr_Format(PyExc_RuntimeError, "Window could not be created! SDL_Error: %s", SDL_GetError());
    }

    return PyCapsule_New(window, "SDL_Window", NULL);
}


static PyObject* window_show(PyObject* self, PyObject* args) {
    PyObject* window_ptr;
    int should_show;

    if (!PyArg_ParseTuple(args, "Op", &window_ptr, &should_show)) {
        return NULL;
    }

    SDL_Window* window = PyCapsule_GetPointer(window_ptr, "SDL_Window");
    if (!window) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid SDL_Window pointer.");
        return NULL;
    }

    if (should_show) {
        SDL_ShowWindow(window);
    } else {
        SDL_HideWindow(window);
    }

    Py_RETURN_NONE;
}


static PyObject* window_close(PyObject* self, PyObject* args) {
    PyObject* window_capsule;

    if (!PyArg_ParseTuple(args, "O", &window_capsule))
        return NULL;

    SDL_Window* window = (SDL_Window*) PyCapsule_GetPointer(window_capsule, "SDL_Window");
    if (!window)
        return NULL;

    SDL_DestroyWindow(window);

    Py_RETURN_NONE;
}


// Function to toggle fullscreen mode
static PyObject* window_fullscreen(PyObject* self, PyObject* args) {
    PyObject* window_ptr;
    int should_fullscreen;

    if (!PyArg_ParseTuple(args, "Op", &window_ptr, &should_fullscreen)) {
        return NULL;
    }

    SDL_Window* window = PyCapsule_GetPointer(window_ptr, "SDL_Window");
    if (!window) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid SDL_Window pointer.");
        return NULL;
    }

    Uint32 fullscreenFlag = SDL_WINDOW_FULLSCREEN_DESKTOP;
    Uint32 isFullscreen = SDL_GetWindowFlags(window) & fullscreenFlag;

    if (should_fullscreen && !isFullscreen) {
        SDL_SetWindowFullscreen(window, fullscreenFlag);
    } else if (!should_fullscreen && isFullscreen) {
        SDL_SetWindowFullscreen(window, 0);
    }

    Py_RETURN_NONE;
}


// Function to get window information
static PyObject* window_info(PyObject* self, PyObject* args) {
    PyObject* window_ptr;

    if (!PyArg_ParseTuple(args, "O", &window_ptr)) {
        return NULL;
    }

    SDL_Window* window = PyCapsule_GetPointer(window_ptr, "SDL_Window");
    if (!window) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid SDL_Window pointer.");
        return NULL;
    }

    PyObject* info_dict = PyDict_New();
    if (!info_dict) {
        return NULL;
    }

    int width, height;
    SDL_GetWindowSize(window, &width, &height);

    int x, y;
    SDL_GetWindowPosition(window, &x, &y);

    Uint32 flags = SDL_GetWindowFlags(window);
    const char* title = SDL_GetWindowTitle(window);

    PyDict_SetItemString(info_dict, "width", PyLong_FromLong(width));
    PyDict_SetItemString(info_dict, "height", PyLong_FromLong(height));
    PyDict_SetItemString(info_dict, "x", PyLong_FromLong(x));
    PyDict_SetItemString(info_dict, "y", PyLong_FromLong(y));
    PyDict_SetItemString(info_dict, "title", PyUnicode_FromString(title));

    // Parse flags
    PyDict_SetItemString(info_dict, "fullscreen", PyBool_FromLong(flags & SDL_WINDOW_FULLSCREEN));
    PyDict_SetItemString(info_dict, "fullscreen_desktop", PyBool_FromLong(flags & SDL_WINDOW_FULLSCREEN_DESKTOP));
    PyDict_SetItemString(info_dict, "resizable", PyBool_FromLong(flags & SDL_WINDOW_RESIZABLE));
    PyDict_SetItemString(info_dict, "minimized", PyBool_FromLong(flags & SDL_WINDOW_MINIMIZED));
    PyDict_SetItemString(info_dict, "maximized", PyBool_FromLong(flags & SDL_WINDOW_MAXIMIZED));
    PyDict_SetItemString(info_dict, "input_grabbed", PyBool_FromLong(flags & SDL_WINDOW_INPUT_GRABBED));
    PyDict_SetItemString(info_dict, "input_focus", PyBool_FromLong(flags & SDL_WINDOW_INPUT_FOCUS));
    PyDict_SetItemString(info_dict, "mouse_focus", PyBool_FromLong(flags & SDL_WINDOW_MOUSE_FOCUS));
    PyDict_SetItemString(info_dict, "shown", PyBool_FromLong(flags & SDL_WINDOW_SHOWN));
    PyDict_SetItemString(info_dict, "hidden", PyBool_FromLong(flags & SDL_WINDOW_HIDDEN));
    PyDict_SetItemString(info_dict, "borderless", PyBool_FromLong(flags & SDL_WINDOW_BORDERLESS));

    return info_dict;
}

static PyObject* window_surface(PyObject* self, PyObject* args) {
    PyObject* window_ptr;

    if (!PyArg_ParseTuple(args, "O", &window_ptr)) {
        return NULL;
    }

    SDL_Window* window = PyCapsule_GetPointer(window_ptr, "SDL_Window");
    if (!window) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid SDL_Window pointer.");
        return NULL;
    }

    SDL_Surface* surface = SDL_GetWindowSurface(window);
    if (!surface) {
        PyErr_SetString(PyExc_RuntimeError, SDL_GetError());
        return NULL;
    }

    return PyCapsule_New(surface, "SDL_Surface", NULL);
}


static PyObject* window_update(PyObject* self, PyObject* args) {
    PyObject* window_ptr;
    if (!PyArg_ParseTuple(args, "O", &window_ptr)) {
        return NULL;
    }

    SDL_Window* window = PyCapsule_GetPointer(window_ptr, "SDL_Window");
    if (!window) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid SDL_Window pointer.");
        return NULL;
    }

    SDL_UpdateWindowSurface(window);
    
    Py_RETURN_NONE;
}


static PyObject* quit(PyObject* self, PyObject* args) {
    if (SDL_WasInit(SDL_INIT_EVERYTHING) != 0) {
        SDL_Quit();
    }

    if (TTF_WasInit() != 0) {
        TTF_Quit();
    }
    
    Py_RETURN_NONE;
}

static PyObject* get_mouse_position(PyObject* self, PyObject* args) {
    int x, y;
    SDL_GetMouseState(&x, &y);
    return Py_BuildValue("(ii)", x, y);
}



static PyObject* mouse_button_to_string(Uint32 button) {
    const char* button_str = NULL;

    switch (button) {
        case SDL_BUTTON_LEFT:
            button_str = "LEFT";
            break;
        case SDL_BUTTON_MIDDLE:
            button_str = "MIDDLE";
            break;
        case SDL_BUTTON_RIGHT:
            button_str = "RIGHT";
            break;
        default:
            button_str = "UNKNOWN";
            break;
    }

    return PyUnicode_FromString(button_str);
}
static PyObject* mouse_buttons_pressed(Uint32 state) {
    PyObject* state_list = PyList_New(0);  // Create a new Python list

    if (state & SDL_BUTTON_LMASK) {
        PyList_Append(state_list, PyUnicode_FromString("LEFT"));
    }
    if (state & SDL_BUTTON_MMASK) {
        PyList_Append(state_list, PyUnicode_FromString("MIDDLE"));
    }
    if (state & SDL_BUTTON_RMASK) {
        PyList_Append(state_list, PyUnicode_FromString("RIGHT"));
    }
    // Add other buttons if needed

    return state_list;  // Return the list of button strings
}

// Global variable to store the time of the last frame
static Uint32 last_frame_time = 0;

void maintain_frame_rate(int fps) {
    Uint32 frame_time = 1000 / fps;  // Time for each frame in milliseconds
    Uint32 current_time, time_since_last_frame, delay_time;

    // Get the current time
    current_time = SDL_GetTicks();

    // Calculate the time since the last frame
    time_since_last_frame = current_time - last_frame_time;

    // If less time has passed than the frame time, delay
    if (time_since_last_frame < frame_time) {
        delay_time = frame_time - time_since_last_frame;
        SDL_Delay(delay_time);
    }

    // Update the time of the last frame
    last_frame_time = SDL_GetTicks();
}


static PyObject* window_for_id(Uint32 windowID) {
    SDL_Window* window = SDL_GetWindowFromID(windowID);
    if (window == NULL) {
        Py_RETURN_NONE;
    } else {
        return PyCapsule_New(window, "SDL_Window", NULL);
    }
}


static PyObject* do_key_pressed_modifiers() {
    SDL_Keymod modState = SDL_GetModState();
    PyObject* modsList = PyList_New(0);  // Create a new Python list

    if (modState & KMOD_SHIFT) {
        PyList_Append(modsList, PyUnicode_FromString("SHIFT"));
    }
    if (modState & KMOD_CTRL) {
        PyList_Append(modsList, PyUnicode_FromString("CTRL"));
    }
    if (modState & KMOD_ALT) {
        PyList_Append(modsList, PyUnicode_FromString("ALT"));
    }
    if (modState & KMOD_GUI) {
        PyList_Append(modsList, PyUnicode_FromString("GUI")); // GUI is the Windows key on Windows, Command key on Macs
    }

    return modsList;  // Return the list of pressed modifier key    
}


// Define a custom event type
#define SDL_USEREVENT_CUSTOM (SDL_USEREVENT + 1)
#define SDL_USEREVENT_SCHEDULED (SDL_USEREVENT + 2)

static Uint32 last_key_time = 0;
static int last_key_code = 0;
const Uint32 double_press_interval = 500; // Threshold in milliseconds for double press

static PyObject* event_enable_text_input(PyObject* self, PyObject* args) {
    int enable;
    if (!PyArg_ParseTuple(args, "p", &enable)) {
        return NULL; // Error in argument parsing
    }
    if (enable)
        SDL_StartTextInput();
    else
        SDL_StopTextInput();
    Py_RETURN_NONE;
}


static PyObject* event_post(PyObject* self, PyObject* args) {
    PyObject* custom_event;
    if (!PyArg_ParseTuple(args, "O!", &PyDict_Type, &custom_event)) {
        return NULL; // Error in argument parsing
    }

    // Create an SDL event
    SDL_Event event;
    SDL_memset(&event, 0, sizeof(event)); // Clear the event structure
    event.type = SDL_USEREVENT_CUSTOM;
    event.user.code = 0;
    event.user.data1 = custom_event;
    event.user.data2 = NULL;

    // Increment reference count to ensure the PyObject stays alive
    Py_INCREF(custom_event);

    // Push the event to the SDL event queue
    if (SDL_PushEvent(&event) < 0) {
        Py_DECREF(custom_event); // Decrement reference count if push fails
        PyErr_SetString(PyExc_RuntimeError, SDL_GetError());
        return NULL;
    }

    Py_RETURN_NONE;
}


typedef struct {
    PyObject* event;
    double interval;
    double total_duration;
    Uint32 start_time;
    SDL_TimerID timer_id;
} TimerData;

static PyObject* convert_sdl_event_to_dict(SDL_Event event) {

    // Handle the scheduled events
    if (event.type == SDL_USEREVENT_SCHEDULED) {
        TimerData* timer_data = (TimerData*)event.user.data1;
        int is_finished = (intptr_t)event.user.data2;

        PyObject* event_dict = PyDict_Copy(timer_data->event);
        if (event_dict) {
            double elapsed_time = (SDL_GetTicks() - timer_data->start_time) / 1000.0;
            PyDict_SetItemString(event_dict, "elapsed", PyFloat_FromDouble(elapsed_time));
            double ratio = is_finished ? 1.0 : fmin(1.0, elapsed_time / timer_data->total_duration);
            PyDict_SetItemString(event_dict, "elapsed_ratio", PyFloat_FromDouble(ratio));
            if (is_finished) {
                Py_DECREF(timer_data->event);
                free(timer_data);
            }
            return event_dict;
        }
    } else if (event.type == SDL_USEREVENT_CUSTOM) {
        return (PyObject*)event.user.data1;
    }


    PyObject* event_dict = PyDict_New();  // Create a new dictionary for this event

    // Convert the event type to a string
    const char* event_type_str = NULL;
    switch (event.type) {
        case SDL_QUIT: event_type_str = "QUIT"; break;
        case SDL_KEYDOWN: event_type_str = "KEYDOWN"; break;
        case SDL_KEYUP: event_type_str = "KEYUP"; break;
        case SDL_TEXTINPUT: event_type_str = "TEXTINPUT"; break;
        case SDL_MOUSEBUTTONDOWN: event_type_str = "MOUSEBUTTONDOWN"; break;
        case SDL_MOUSEBUTTONUP: event_type_str = "MOUSEBUTTONUP"; break;
        case SDL_MOUSEMOTION: event_type_str = "MOUSEMOTION"; break;
        case SDL_MOUSEWHEEL: event_type_str = "MOUSEWHEEL"; break;
        case SDL_WINDOWEVENT: event_type_str = "WINDOWEVENT"; break;
        default: event_type_str = "UNKNOWN"; break;
    }
    PyDict_SetItemString(event_dict, "type_id", PyLong_FromLong(event.type));
    PyDict_SetItemString(event_dict, "type", PyUnicode_FromString(event_type_str));

    // Handle keyboard events
    if (event.type == SDL_KEYDOWN || event.type == SDL_KEYUP) {
        const char* keyName = SDL_GetKeyName(event.key.keysym.sym);
        PyDict_SetItemString(event_dict, "key", PyUnicode_FromString(keyName));
        PyDict_SetItemString(event_dict, "modifiers", do_key_pressed_modifiers());
        PyDict_SetItemString(event_dict, "state", PyLong_FromLong(event.key.state));
        PyDict_SetItemString(event_dict, "repeat", PyLong_FromLong(event.key.repeat));
        PyDict_SetItemString(event_dict, "window", window_for_id(event.key.windowID));
    }

    if (event.type == SDL_TEXTINPUT) {
        PyDict_SetItemString(event_dict, "text", PyUnicode_FromString(event.text.text));
        PyDict_SetItemString(event_dict, "window", window_for_id(event.text.windowID));
        PyDict_SetItemString(event_dict, "modifiers", do_key_pressed_modifiers());
    }

    // Handle mouse button events
    if (event.type == SDL_MOUSEBUTTONDOWN || event.type == SDL_MOUSEBUTTONUP) {
        PyDict_SetItemString(event_dict, "button", mouse_button_to_string(event.button.button));
        PyDict_SetItemString(event_dict, "x", PyLong_FromLong(event.button.x));
        PyDict_SetItemString(event_dict, "y", PyLong_FromLong(event.button.y));
        PyDict_SetItemString(event_dict, "clicks", PyLong_FromLong(event.button.clicks));
        PyObject* stateList = mouse_buttons_pressed(event.button.state);
        PyDict_SetItemString(event_dict, "state", stateList);
        PyDict_SetItemString(event_dict, "window", window_for_id(event.button.windowID));
        PyDict_SetItemString(event_dict, "modifiers", do_key_pressed_modifiers());
        Py_DECREF(stateList);
    }

    // Handle mouse movement events
    if (event.type == SDL_MOUSEMOTION) {
        PyDict_SetItemString(event_dict, "x", PyLong_FromLong(event.motion.x));
        PyDict_SetItemString(event_dict, "y", PyLong_FromLong(event.motion.y));
        PyDict_SetItemString(event_dict, "xrel", PyLong_FromLong(event.motion.xrel));
        PyDict_SetItemString(event_dict, "yrel", PyLong_FromLong(event.motion.yrel));
        PyDict_SetItemString(event_dict, "window", window_for_id(event.motion.windowID));
        PyObject* stateList = mouse_buttons_pressed(event.motion.state);
        PyDict_SetItemString(event_dict, "state", stateList);
        PyDict_SetItemString(event_dict, "modifiers", do_key_pressed_modifiers());
        Py_DECREF(stateList);
    }

    // Handle mouse wheel events
    if (event.type == SDL_MOUSEWHEEL) {
        PyDict_SetItemString(event_dict, "x", PyLong_FromLong(event.wheel.x));
        PyDict_SetItemString(event_dict, "y", PyLong_FromLong(event.wheel.y));
        PyDict_SetItemString(event_dict, "direction", PyLong_FromLong(event.wheel.direction));
        PyDict_SetItemString(event_dict, "window", window_for_id(event.wheel.windowID));
        PyDict_SetItemString(event_dict, "modifiers", do_key_pressed_modifiers());
    }

    // Handle window events
    if (event.type == SDL_WINDOWEVENT) {
        PyDict_SetItemString(event_dict, "data1", PyLong_FromLong(event.window.data1));
        PyDict_SetItemString(event_dict, "data2", PyLong_FromLong(event.window.data2));
        PyDict_SetItemString(event_dict, "window", window_for_id(event.window.windowID));
        // Convert the window event type to a string
        const char* window_event_str = NULL;
        switch (event.window.event) {
            case SDL_WINDOWEVENT_SHOWN: window_event_str = "SHOWN"; break;
            case SDL_WINDOWEVENT_HIDDEN: window_event_str = "HIDDEN"; break;
            case SDL_WINDOWEVENT_EXPOSED: window_event_str = "EXPOSED"; break;
            case SDL_WINDOWEVENT_MOVED: window_event_str = "MOVED"; break;
            case SDL_WINDOWEVENT_RESIZED: window_event_str = "RESIZED"; break;
            case SDL_WINDOWEVENT_SIZE_CHANGED: window_event_str = "SIZE_CHANGED"; break;
            case SDL_WINDOWEVENT_MINIMIZED: window_event_str = "MINIMIZED"; break;
            case SDL_WINDOWEVENT_MAXIMIZED: window_event_str = "MAXIMIZED"; break;
            case SDL_WINDOWEVENT_RESTORED: window_event_str = "RESTORED"; break;
            case SDL_WINDOWEVENT_ENTER: window_event_str = "ENTER"; break;
            case SDL_WINDOWEVENT_LEAVE: window_event_str = "LEAVE"; break;
            case SDL_WINDOWEVENT_FOCUS_GAINED: window_event_str = "FOCUS_GAINED"; break;
            case SDL_WINDOWEVENT_FOCUS_LOST: window_event_str = "FOCUS_LOST"; break;
            case SDL_WINDOWEVENT_CLOSE: window_event_str = "CLOSE"; break;
            // Add any additional window events you need to handle here
            default: window_event_str = "UNKNOWN_WINDOW_EVENT"; break;
        }
        PyDict_SetItemString(event_dict, "event", PyUnicode_FromString(window_event_str));
    }

    return event_dict;
}

PyObject* check_double_keydowns(SDL_Event* event) {
    if (event->type == SDL_KEYDOWN) {
        Uint32 current_time = SDL_GetTicks();
        int current_key_code = event->key.keysym.sym;
        
        // Check for double press
        if (current_key_code == last_key_code && (current_time - last_key_time <= double_press_interval)) {
            // Handle double press event
            PyObject* event_dict = PyDict_New();
            const char* keyName = SDL_GetKeyName(event->key.keysym.sym);
            PyDict_SetItemString(event_dict, "type_id", PyLong_FromLong(event->type));
            PyDict_SetItemString(event_dict, "type", PyUnicode_FromString("KEYDOWNx2"));
            PyDict_SetItemString(event_dict, "key", PyUnicode_FromString(keyName));
            PyDict_SetItemString(event_dict, "modifiers", do_key_pressed_modifiers());
            PyDict_SetItemString(event_dict, "state", PyLong_FromLong(event->key.state));
            PyDict_SetItemString(event_dict, "repeat", PyLong_FromLong(event->key.repeat));
            PyDict_SetItemString(event_dict, "window", window_for_id(event->key.windowID));
            last_key_code = -1; // We don't want emit another KEYDOWNx2 if the same key is pressed a third time.
            return event_dict;
        } else {
            // Update last key press information
            last_key_time = current_time;
            last_key_code = current_key_code;
        }
    }
    return NULL;
} 

Uint32 timer_callback(Uint32 interval, void* param) {
    TimerData* timer_data = (TimerData*)param;
    Uint32 current_time = SDL_GetTicks();
    double elapsed_time = (current_time - timer_data->start_time) / 1000.0;

    SDL_Event sdl_event;
    SDL_memset(&sdl_event, 0, sizeof(sdl_event));
    sdl_event.type = SDL_USEREVENT_SCHEDULED;
    sdl_event.user.code = 0;
    sdl_event.user.data1 = timer_data;

    if (elapsed_time >= timer_data->total_duration) {
        sdl_event.user.data2 = (void*)1;  // Signal the end of the timer
        SDL_RemoveTimer(timer_data->timer_id);
    } else {
        sdl_event.user.data2 = (void*)0;  // Signal that the timer is still running
    }

    SDL_PushEvent(&sdl_event);

    return elapsed_time >= timer_data->total_duration ? 0 : (Uint32)(timer_data->interval * 1000);
}

static PyObject* event_schedule(PyObject* self, PyObject* args) {
    double interval;
    double total_duration;
    PyObject* event;

    if (!PyArg_ParseTuple(args, "ddO", &interval, &total_duration, &event)) {
        return NULL;
    }

    if (!ensure_sdl_initialized()) {
        return NULL;
    }

    TimerData* timer_data = (TimerData*)malloc(sizeof(TimerData));
    if (!timer_data) {
        return PyErr_NoMemory();
    }

    Py_INCREF(event);

    timer_data->event = event;
    timer_data->interval = interval;
    timer_data->total_duration = total_duration;
    timer_data->start_time = SDL_GetTicks();

    timer_data->timer_id = SDL_AddTimer((Uint32)(interval * 1000), timer_callback, timer_data);
    if (!timer_data->timer_id) {
        Py_DECREF(event);
        free(timer_data);
        return PyErr_Format(PyExc_RuntimeError, "SDL_AddTimer Error: %s", SDL_GetError());
    }

    Py_RETURN_NONE;
}

static PyObject* event_wait(PyObject* self, PyObject* args) {
    int fps = 0;
    if (!PyArg_ParseTuple(args, "|i", &fps)) {
        return NULL; // Error in argument parsing
    }

    if (fps > 0)
        maintain_frame_rate(fps); // Wait the needed time to maintain the framerate
    else
        SDL_WaitEvent(NULL); // Wait indefinitely until there is at least one event in the queue

    SDL_Event event;
    PyObject* event_list = PyList_New(0);  // Create a new Python list

    while (SDL_PollEvent(&event)) {
        PyObject* x2_event_dict = check_double_keydowns(&event);
        if (x2_event_dict) {
            PyList_Append(event_list, x2_event_dict);
            Py_DECREF(x2_event_dict);
        }

        PyObject* event_dict = convert_sdl_event_to_dict(event);
        PyList_Append(event_list, event_dict);
        Py_DECREF(event_dict);
    }
    return event_list;  // Return the list of event dictionaries
}


static PyObject* key_pressed_modifiers(PyObject* self, PyObject* args) {
    return do_key_pressed_modifiers();
}


int parse_color(PyObject* tuple_obj, int* r, int* g, int* b, int* a) {
    *a = 255;  // Assuming a default alpha of 255 (opaque) if not provided.

    if (!PyTuple_Check(tuple_obj)) {
        PyErr_SetString(PyExc_TypeError, "Expected a tuple.");
        return 0;  // Failed
    }

    Py_ssize_t tuple_size = PyTuple_Size(tuple_obj);

    if (tuple_size == 3) {
        if (!PyArg_ParseTuple(tuple_obj, "iii", r, g, b)) {
            return 0;  // Failed
        }
    } else if (tuple_size == 4) {
        if (!PyArg_ParseTuple(tuple_obj, "iiii", r, g, b, a)) {
            return 0;  // Failed
        }
    } else {
        PyErr_SetString(PyExc_ValueError, "Expected a tuple with 3 or 4 integers.");
        return 0;  // Failed
    }

    return 1;  // Success
}



static PyObject* draw_clear(PyObject* self, PyObject* args) {
    PyObject* surface_ptr;
    PyObject* color_tuple;

    if (!PyArg_ParseTuple(args, "OO", &surface_ptr, &color_tuple))
        return NULL;

    int r, g, b, a;
    if (!parse_color(color_tuple, &r, &g, &b, &a)) {
        return NULL;  // Error has been set inside parse_color
    }

    SDL_Surface* surface = PyCapsule_GetPointer(surface_ptr, "SDL_Surface");
    if (!surface) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid SDL_Surface pointer.");
        return NULL;
    }

    SDL_FillRect(surface, NULL, SDL_MapRGBA(surface->format, r, g, b, a));

    Py_RETURN_NONE;
}

static void draw_pixel_wo_alpha(SDL_Surface* surface, int x, int y, Uint32 color) {
    if (x >= 0 && x < surface->w && y >= 0 && y < surface->h) {
        ((Uint32*)surface->pixels)[y * (surface->pitch / sizeof(Uint32)) + x] = color;
    }
}


static void draw_pixel_w_alpha(SDL_Surface* surface, int x, int y, Uint8 r, Uint8 g, Uint8 b, Uint8 a) {
    if (a == 0)
        return;
    int bpp = surface->format->BytesPerPixel;
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    // Extract the current color components
    Uint8 curr_r, curr_g, curr_b;
    SDL_GetRGB(*(Uint32*)p, surface->format, &curr_r, &curr_g, &curr_b);

    // Alpha blending using integer arithmetic
    Uint8 blendedR = (Uint8)(((r * a) + (curr_r * (255 - a))) >> 8);
    Uint8 blendedG = (Uint8)(((g * a) + (curr_g * (255 - a))) >> 8);
    Uint8 blendedB = (Uint8)(((b * a) + (curr_b * (255 - a))) >> 8);

    *(Uint32 *)p = SDL_MapRGBA(surface->format, blendedR, blendedG, blendedB, 255); // We set the result alpha to 255, assuming the surface doesn't handle transparency.
}


static PyObject* draw_pixels(PyObject* self, PyObject* args) {
    PyObject* surface_ptr;
    PyObject* np_array;
    PyObject* color_obj;

    if (!PyArg_ParseTuple(args, "OOO", &surface_ptr, &np_array, &color_obj))
        return NULL;

    SDL_Surface* surface = PyCapsule_GetPointer(surface_ptr, "SDL_Surface");
    if (!surface) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid SDL_Surface pointer.");
        return NULL;
    }

    PyArrayObject* np_arr = (PyArrayObject*)PyArray_FROM_OTF(np_array, NPY_INT, NPY_ARRAY_IN_ARRAY);
    if (np_arr == NULL)
        return NULL;

    int len = PyArray_DIM(np_arr, 0);

    if (PyTuple_Check(color_obj)) {
        // Single color mode
        int r, g, b, a;
        if (!parse_color(color_obj, &r, &g, &b, &a)) {
            Py_DECREF(np_arr);
            return NULL;  // Error has been set inside parse_color
        }
        Uint32 color = SDL_MapRGBA(surface->format, r, g, b, a);
        for (int i = 0; i < len; ++i) {
            int* pixel_position = (int*)PyArray_GETPTR2(np_arr, i, 0);
            int x = pixel_position[0];
            int y = pixel_position[1];
            if (a == 255) {
                draw_pixel_wo_alpha(surface, x, y, color);
            } else {
                draw_pixel_w_alpha(surface, x, y, r, g, b, a);
            }
        }
    } else if (PyArray_Check(color_obj)) {
        // Array of colors mode
        PyArrayObject* color_arr = (PyArrayObject*)PyArray_FROM_OTF(color_obj, NPY_INT, NPY_ARRAY_IN_ARRAY);
        if (color_arr == NULL) {
            Py_DECREF(np_arr);
            return NULL;
        }

        int color_len = PyArray_DIM(color_arr, 0);
        if (color_len != len) {
            Py_DECREF(np_arr);
            Py_DECREF(color_arr);
            return PyErr_Format(PyExc_ValueError, "The color array length must match the number of pixels");
        }

        for (int i = 0; i < len; ++i) {
            int* pixel_position = (int*)PyArray_GETPTR2(np_arr, i, 0);
            int x = pixel_position[0];
            int y = pixel_position[1];

            int* color = (int*)PyArray_GETPTR1(color_arr, i);
            int r = color[0];
            int g = color[1];
            int b = color[2];
            int a = (PyArray_DIM(color_arr, 1) == 4) ? color[3] : 255;

            if (a == 255) {
                Uint32 sdl_color = SDL_MapRGBA(surface->format, r, g, b, a);
                draw_pixel_wo_alpha(surface, x, y, sdl_color);
            } else {
                draw_pixel_w_alpha(surface, x, y, r, g, b, a);
            }
        }
        Py_DECREF(color_arr);
    } else {
        Py_DECREF(np_arr);
        return PyErr_Format(PyExc_TypeError, "Color must be either a tuple or a numpy array");
    }

    Py_DECREF(np_arr);
    Py_RETURN_NONE;
}

#define INSIDE 0 // 0000
#define LEFT 1   // 0001
#define RIGHT 2  // 0010
#define BOTTOM 4 // 0100
#define TOP 8    // 1000

// Function to compute the region code for a point (x, y)
static int compute_code(int x, int y, int width, int height) {
    int code = INSIDE;

    if (x < 0) code |= LEFT;
    else if (x >= width) code |= RIGHT;
    if (y < 0) code |= BOTTOM;
    else if (y >= height) code |= TOP;

    return code;
}

// Cohen-Sutherland clipping algorithm
static int cohen_sutherland_clip(int* x1, int* y1, int* x2, int* y2, int width, int height) {
    int code1 = compute_code(*x1, *y1, width, height);
    int code2 = compute_code(*x2, *y2, width, height);
    int accept = 0;

    while (1) {
        if ((code1 == 0) && (code2 == 0)) {
            accept = 1;
            break;
        } else if (code1 & code2) {
            break;
        } else {
            int code_out;
            int x = 0, y = 0;

            if (code1 != 0)
                code_out = code1;
            else
                code_out = code2;

            if (code_out & TOP) {
                x = *x1 + (*x2 - *x1) * (height - 1 - *y1) / (*y2 - *y1);
                y = height - 1;
            } else if (code_out & BOTTOM) {
                x = *x1 + (*x2 - *x1) * (0 - *y1) / (*y2 - *y1);
                y = 0;
            } else if (code_out & RIGHT) {
                y = *y1 + (*y2 - *y1) * (width - 1 - *x1) / (*x2 - *x1);
                x = width - 1;
            } else if (code_out & LEFT) {
                y = *y1 + (*y2 - *y1) * (0 - *x1) / (*x2 - *x1);
                x = 0;
            }

            if (code_out == code1) {
                *x1 = x;
                *y1 = y;
                code1 = compute_code(*x1, *y1, width, height);
            } else {
                *x2 = x;
                *y2 = y;
                code2 = compute_code(*x2, *y2, width, height);
            }
        }
    }

    return accept;
}

static void draw_line_w_alpha(SDL_Surface* surface, int x1, int y1, int x2, int y2, Uint8 r, Uint8 g, Uint8 b, Uint8 a) {
    if (!cohen_sutherland_clip(&x1, &y1, &x2, &y2, surface->w, surface->h)) {
        return;
    }

    int dx = abs(x2 - x1);
    int sx = x1 < x2 ? 1 : -1;
    int dy = -abs(y2 - y1);
    int sy = y1 < y2 ? 1 : -1;
    int err = dx + dy;

    while (1) {
        draw_pixel_w_alpha(surface, x1, y1, r, g, b, a);
        if (x1 == x2 && y1 == y2) break;
        int e2 = 2 * err;
        if (e2 >= dy) {
            err += dy;
            x1 += sx;
        }
        if (e2 <= dx) {
            err += dx;
            y1 += sy;
        }
    }
}

static void draw_line_wo_alpha(SDL_Surface* surface, int x1, int y1, int x2, int y2, Uint32 color) {
    if (!cohen_sutherland_clip(&x1, &y1, &x2, &y2, surface->w, surface->h)) {
        return;
    }

    int dx = abs(x2 - x1);
    int sx = x1 < x2 ? 1 : -1;
    int dy = -abs(y2 - y1);
    int sy = y1 < y2 ? 1 : -1;
    int err = dx + dy;

    while (1) {
        draw_pixel_wo_alpha(surface, x1, y1, color);
        if (x1 == x2 && y1 == y2) break;
        int e2 = 2 * err;
        if (e2 >= dy) {
            err += dy;
            x1 += sx;
        }
        if (e2 <= dx) {
            err += dx;
            y1 += sy;
        }
    }
}

static PyObject* draw_lines(PyObject* self, PyObject* args) {
    PyObject* surface_ptr;
    PyObject* np_array;
    PyObject* color_obj;

    if (!PyArg_ParseTuple(args, "OOO", &surface_ptr, &np_array, &color_obj))
        return NULL;

    SDL_Surface* surface = PyCapsule_GetPointer(surface_ptr, "SDL_Surface");
    if (!surface) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid SDL_Surface pointer.");
        return NULL;
    }

    PyArrayObject* np_arr = (PyArrayObject*)PyArray_FROM_OTF(np_array, NPY_INT, NPY_ARRAY_IN_ARRAY);
    if (np_arr == NULL)
        return NULL;

    int len = PyArray_DIM(np_arr, 0);

    if (PyTuple_Check(color_obj)) {
        // Single color mode
        int r, g, b, a;
        if (!parse_color(color_obj, &r, &g, &b, &a)) {
            Py_DECREF(np_arr);
            return NULL;  // Error has been set inside parse_color
        }

        if (a == 255) {
            Uint32 color = SDL_MapRGBA(surface->format, r, g, b, a);
            for (int i = 0; i < len; ++i) {
                int* row_data = (int*)PyArray_GETPTR2(np_arr, i, 0);
                int x1 = row_data[0];
                int y1 = row_data[1];
                int x2 = row_data[2];
                int y2 = row_data[3];
                draw_line_wo_alpha(surface, x1, y1, x2, y2, color);
            }
        } else {
            for (int i = 0; i < len; ++i) {
                int* row_data = (int*)PyArray_GETPTR2(np_arr, i, 0);
                int x1 = row_data[0];
                int y1 = row_data[1];
                int x2 = row_data[2];
                int y2 = row_data[3];
                draw_line_w_alpha(surface, x1, y1, x2, y2, r, g, b, a);
            }
        }
    } else if (PyArray_Check(color_obj)) {
        // Array of colors mode
        PyArrayObject* color_arr = (PyArrayObject*)PyArray_FROM_OTF(color_obj, NPY_INT, NPY_ARRAY_IN_ARRAY);
        if (color_arr == NULL) {
            Py_DECREF(np_arr);
            return NULL;
        }

        int color_len = PyArray_DIM(color_arr, 0);
        if (color_len != len) {
            Py_DECREF(np_arr);
            Py_DECREF(color_arr);
            return PyErr_Format(PyExc_ValueError, "The color array length must match the number of lines");
        }

        for (int i = 0; i < len; ++i) {
            int* row_data = (int*)PyArray_GETPTR2(np_arr, i, 0);
            int x1 = row_data[0];
            int y1 = row_data[1];
            int x2 = row_data[2];
            int y2 = row_data[3];

            int* color = (int*)PyArray_GETPTR1(color_arr, i);
            int r = color[0];
            int g = color[1];
            int b = color[2];
            int a = (PyArray_DIM(color_arr, 1) == 4) ? color[3] : 255;

            if (a == 255) {
                Uint32 sdl_color = SDL_MapRGBA(surface->format, r, g, b, a);
                draw_line_wo_alpha(surface, x1, y1, x2, y2, sdl_color);
            } else {
                draw_line_w_alpha(surface, x1, y1, x2, y2, r, g, b, a);
            }
        }

        Py_DECREF(color_arr);
    } else {
        Py_DECREF(np_arr);
        return PyErr_Format(PyExc_TypeError, "Color must be either a tuple or a numpy array");
    }

    Py_DECREF(np_arr);

    Py_RETURN_NONE;
}


static PyObject* draw_polyline(PyObject* self, PyObject* args) {
    PyObject* surface_ptr;
    PyObject* np_array;
    PyObject* color_tuple;

    if (!PyArg_ParseTuple(args, "OOO", &surface_ptr, &np_array, &color_tuple))
        return NULL;

    int r, g, b, a;
    if (!parse_color(color_tuple, &r, &g, &b, &a)) {
        return NULL;  // Error has been set inside parse_color
    }

    SDL_Surface* surface = PyCapsule_GetPointer(surface_ptr, "SDL_Surface");
    if (!surface) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid SDL_Surface pointer.");
        return NULL;
    }

    PyArrayObject* np_arr = (PyArrayObject*)PyArray_FROM_OTF(np_array, NPY_INT, NPY_ARRAY_IN_ARRAY);
    if (np_arr == NULL)
        return NULL;

    int len = PyArray_DIM(np_arr, 0);

    if (len < 2) {
        Py_DECREF(np_arr);
        return PyErr_Format(PyExc_ValueError, "Need at least 2 points to draw a curve");
    }

    if (a == 255) {
        Uint32 color = SDL_MapRGBA(surface->format, r, g, b, a);
        for (int i = 0; i < len - 1; ++i) {
            int* start_point = (int*)PyArray_GETPTR2(np_arr, i, 0);
            int* end_point = (int*)PyArray_GETPTR2(np_arr, i + 1, 0);
            int x1 = start_point[0];
            int y1 = start_point[1];
            int x2 = end_point[0];
            int y2 = end_point[1];
            draw_line_wo_alpha(surface, x1, y1, x2, y2, color);
        }
    } else {
        for (int i = 0; i < len - 1; ++i) {
            int* start_point = (int*)PyArray_GETPTR2(np_arr, i, 0);
            int* end_point = (int*)PyArray_GETPTR2(np_arr, i + 1, 0);
            int x1 = start_point[0];
            int y1 = start_point[1];
            int x2 = end_point[0];
            int y2 = end_point[1];
            draw_line_w_alpha(surface, x1, y1, x2, y2, r, g, b, a);
        }
    }
    Py_DECREF(np_arr);

    Py_RETURN_NONE;
}


static void fill_rect_wo_alpha(SDL_Surface* surface, int x, int y, int w, int h, Uint32 color) {
    int max_x = x + w;
    int max_y = y + h;

    // Ensure we don't start or finish outside the surface boundaries
    if (x < 0) x = 0;
    if (y < 0) y = 0;
    if (max_x > surface->w) max_x = surface->w;
    if (max_y > surface->h) max_y = surface->h;

    // Adjust width and height based on the clipped boundaries
    w = max_x - x;
    h = max_y - y;

    for (int j = y; j < y + h; j++) {
        for (int i = x; i < x + w; i++) {
            draw_pixel_wo_alpha(surface, i, j, color);
        }
    }
}

static void fill_rect_w_alpha(SDL_Surface* surface, int x, int y, int w, int h, Uint8 r, Uint8 g, Uint8 b, Uint8 a) {
    int max_x = x + w;
    int max_y = y + h;

    // Ensure we don't start or finish outside the surface boundaries
    if (x < 0) x = 0;
    if (y < 0) y = 0;
    if (max_x > surface->w) max_x = surface->w;
    if (max_y > surface->h) max_y = surface->h;

    // Adjust width and height based on the clipped boundaries
    w = max_x - x;
    h = max_y - y;

    for (int j = y; j < y + h; j++) {
        for (int i = x; i < x + w; i++) {
            draw_pixel_w_alpha(surface, i, j, r, g, b, a);
        }
    }
}

void draw_border_wo_alpha(SDL_Surface* surface, int x, int y, int w, int h, Uint32 color, int border_width) {
    for (int i = 0; i < border_width; ++i) {
        // Top border
        draw_line_wo_alpha(surface, x + i + 1, y + i, x + w - 2 - i, y + i, color);
        // Bottom border
        draw_line_wo_alpha(surface, x + i + 1, y + h - 1 - i, x + w - 2 - i, y + h - 1 - i, color);
        // Left border
        draw_line_wo_alpha(surface, x + i, y + i, x + i, y + h - 1 - i, color);
        // Right border
        draw_line_wo_alpha(surface, x + w - 1 - i, y + i, x + w - 1 - i, y + h - 1 - i, color);
    }
}


void draw_border_w_alpha(SDL_Surface* surface, int x, int y, int w, int h, Uint8 r, Uint8 g, Uint8 b, Uint8 a, int border_width) {
    for (int i = 0; i < border_width; ++i) {
        // Top border
        draw_line_w_alpha(surface, x + i + 1, y + i, x + w - 2 - i, y + i, r, g, b, a);
        // Bottom border
        draw_line_w_alpha(surface, x + i + 1, y + h - 1 - i, x + w - 2 - i, y + h - 1 - i, r, g, b, a);
        // Left border
        draw_line_w_alpha(surface, x + i, y + i, x + i, y + h - 1 - i, r, g, b, a);
        // Right border
        draw_line_w_alpha(surface, x + w - 1 - i, y + i, x + w - 1 - i, y + h - 1 - i, r, g, b, a);
    }
}

static PyObject* draw_rectangles(PyObject* self, PyObject* args) {
    PyObject* surface_ptr;
    PyObject* np_array;
    PyObject* color_tuple;

    if (!PyArg_ParseTuple(args, "OOO", &surface_ptr, &np_array, &color_tuple))
        return NULL;

    int r, g, b, a;
    if (!parse_color(color_tuple, &r, &g, &b, &a)) {
        return NULL;  // Error has been set inside parse_color
    }

    SDL_Surface* surface = PyCapsule_GetPointer(surface_ptr, "SDL_Surface");
    if (!surface) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid SDL_Surface pointer.");
        return NULL;
    }

    PyArrayObject* np_arr = (PyArrayObject*)PyArray_FROM_OTF(np_array, NPY_INT, NPY_ARRAY_IN_ARRAY);
    if (np_arr == NULL)
        return NULL;

    int len = PyArray_DIM(np_arr, 0);

    if (a == 255) {
        Uint32 color = SDL_MapRGBA(surface->format, r, g, b, a);
        for (int i = 0; i < len; ++i) {
            int* box_data = (int*)PyArray_GETPTR2(np_arr, i, 0);
            int x = box_data[0];
            int y = box_data[1];
            int w = box_data[2];
            int h = box_data[3];
            int border_width = box_data[4];
            if (border_width == 0) {
                fill_rect_wo_alpha(surface, x, y, w, h, color);
            } else {
                draw_border_wo_alpha(surface, x, y, w, h, color, border_width);
            }
        }
    } else {
        for (int i = 0; i < len; ++i) {
            int* box_data = (int*)PyArray_GETPTR2(np_arr, i, 0);
            int x = box_data[0];
            int y = box_data[1];
            int w = box_data[2];
            int h = box_data[3];
            int border_width = box_data[4];
            if (border_width == 0) {
                fill_rect_w_alpha(surface, x, y, w, h, r, g, b, a);
            } else {
                draw_border_w_alpha(surface, x, y, w, h, r, g, b, a, border_width);
            }
        }
    }

    Py_DECREF(np_arr);
    Py_RETURN_NONE;
}


static void draw_filled_triangle_wo_alpha(SDL_Surface* surface, int* pts, int n, Uint32 color) {
    // Simple scanline algorithm to fill the triangle
    int minY = pts[1], maxY = pts[1];
    for (int i = 1; i < n; i++) {
        if (pts[2 * i + 1] < minY) minY = pts[2 * i + 1];
        if (pts[2 * i + 1] > maxY) maxY = pts[2 * i + 1];
    }
    
    minY = minY < 0 ? 0 : minY; // Clip minY to surface bounds
    maxY = maxY >= surface->h ? surface->h - 1 : maxY; // Clip maxY to surface bounds

    for (int y = minY; y <= maxY; y++) {
        int nodes = 0, nodeX[6];
        int j = 2 * (n - 1);
        for (int i = 0; i < n; i++) {
            if (((pts[2 * i + 1] < y) && (pts[j + 1] >= y)) || ((pts[j + 1] < y) && (pts[2 * i + 1] >= y))) {
                nodeX[nodes++] = pts[2 * i] + (y - pts[2 * i + 1]) * (pts[j] - pts[2 * i]) / (pts[j + 1] - pts[2 * i + 1]);
            }
            j = 2 * i;
        }
        for (int i = 0; i < nodes - 1; i += 2) {
            if (nodeX[i] > nodeX[i + 1]) {
                int temp = nodeX[i];
                nodeX[i] = nodeX[i + 1];
                nodeX[i + 1] = temp;
            }
            for (int x = nodeX[i]; x <= nodeX[i + 1]; x++) { // Changed '<' to '<=' to include end pixel
                if (x >= 0 && x < surface->w && y >= 0 && y < surface->h) { // Ensure x and y are within bounds
                    Uint32* bufp = (Uint32*)((Uint8*)surface->pixels + y * surface->pitch + x * surface->format->BytesPerPixel);
                    *bufp = color;
                }
            }
        }
    }
}
static void draw_filled_triangle_w_alpha(SDL_Surface* surface, int* pts, int n, int r, int g, int b, int a) {
    // Simple scanline algorithm to fill the triangle with alpha blending
    int minY = pts[1], maxY = pts[1];
    for (int i = 1; i < n; i++) {
        if (pts[2 * i + 1] < minY) minY = pts[2 * i + 1];
        if (pts[2 * i + 1] > maxY) maxY = pts[2 * i + 1];
    }

    minY = minY < 0 ? 0 : minY; // Clip minY to surface bounds
    maxY = maxY >= surface->h ? surface->h - 1 : maxY; // Clip maxY to surface bounds

    for (int y = minY; y <= maxY; y++) {
        int nodes = 0, nodeX[6];
        int j = 2 * (n - 1);
        for (int i = 0; i < n; i++) {
            if (((pts[2 * i + 1] < y) && (pts[j + 1] >= y)) || ((pts[j + 1] < y) && (pts[2 * i + 1] >= y))) {
                nodeX[nodes++] = pts[2 * i] + (y - pts[2 * i + 1]) * (pts[j] - pts[2 * i]) / (pts[j + 1] - pts[2 * i + 1]);
            }
            j = 2 * i;
        }
        for (int i = 0; i < nodes - 1; i += 2) {
            if (nodeX[i] > nodeX[i + 1]) {
                int temp = nodeX[i];
                nodeX[i] = nodeX[i + 1];
                nodeX[i + 1] = temp;
            }
            for (int x = nodeX[i]; x <= nodeX[i + 1]; x++) { // Changed '<' to '<=' to include end pixel
                if (x >= 0 && x < surface->w && y >= 0 && y < surface->h) { // Ensure x and y are within bounds
                    draw_pixel_w_alpha(surface, x, y, r, g, b, a);
                }
            }
        }
    }
}



static void draw_triangle_border_wo_alpha(SDL_Surface* surface, int* pts, Uint32 color) {
    draw_line_wo_alpha(surface, pts[0], pts[1], pts[2], pts[3], color);
    draw_line_wo_alpha(surface, pts[2], pts[3], pts[4], pts[5], color);
    draw_line_wo_alpha(surface, pts[4], pts[5], pts[0], pts[1], color);
}

static void draw_triangle_border_w_alpha(SDL_Surface* surface, int* pts, int r, int g, int b, int a) {
    draw_line_w_alpha(surface, pts[0], pts[1], pts[2], pts[3], r, g, b, a);
    draw_line_w_alpha(surface, pts[2], pts[3], pts[4], pts[5], r, g, b, a);
    draw_line_w_alpha(surface, pts[4], pts[5], pts[0], pts[1], r, g, b, a);
}


static PyObject* draw_triangles(PyObject* self, PyObject* args) {
    PyObject* surface_ptr;
    PyObject* np_array;
    PyObject* color_obj;

    if (!PyArg_ParseTuple(args, "OOO", &surface_ptr, &np_array, &color_obj))
        return NULL;

    SDL_Surface* surface = PyCapsule_GetPointer(surface_ptr, "SDL_Surface");
    if (!surface) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid SDL_Surface pointer.");
        return NULL;
    }

    PyArrayObject* np_arr = (PyArrayObject*)PyArray_FROM_OTF(np_array, NPY_INT, NPY_ARRAY_IN_ARRAY);
    if (np_arr == NULL)
        return NULL;

    int width = PyArray_DIM(np_arr, 1);
    if (width != 7) {
        Py_DECREF(np_arr);
        return PyErr_Format(PyExc_ValueError, "Each triangle must have three coordinates and a width, thus a Nx7 numpy array");
    }

    int len = PyArray_DIM(np_arr, 0);

    if (PyTuple_Check(color_obj)) {
        // Single color mode
        int r, g, b, a;
        if (!parse_color(color_obj, &r, &g, &b, &a)) {
            Py_DECREF(np_arr);
            return NULL;  // Error has been set inside parse_color
        }

        for (int i = 0; i < len; i++) {
            int* pts = (int*)PyArray_GETPTR2(np_arr, i, 0);
            int tri_width = pts[6];
            if (tri_width > 1 && a != 255) {
                Py_DECREF(np_arr);
                return PyErr_Format(PyExc_ValueError, "Alpha blending is not supported for width > 1");
            }

            Uint32 color = SDL_MapRGBA(surface->format, r, g, b, a);
            if (tri_width == 0) {
                // Draw filled triangle
                if (a == 255) {
                    draw_filled_triangle_wo_alpha(surface, pts, 3, color);
                } else {
                    draw_filled_triangle_w_alpha(surface, pts, 3, r, g, b, a);
                }
            } else {
                // Draw triangle border
                if (a == 255) {
                    draw_triangle_border_wo_alpha(surface, pts, color);
                } else {
                    draw_triangle_border_w_alpha(surface, pts, r, g, b, a);
                }
            }
        }
    } else if (PyArray_Check(color_obj)) {
        // Array of colors mode
        PyArrayObject* color_arr = (PyArrayObject*)PyArray_FROM_OTF(color_obj, NPY_INT, NPY_ARRAY_IN_ARRAY);
        if (color_arr == NULL) {
            Py_DECREF(np_arr);
            return NULL;
        }

        int color_len = PyArray_DIM(color_arr, 0);
        if (color_len != len) {
            Py_DECREF(np_arr);
            Py_DECREF(color_arr);
            return PyErr_Format(PyExc_ValueError, "The color array length must match the number of triangles");
        }

        for (int i = 0; i < len; i++) {
            int* pts = (int*)PyArray_GETPTR2(np_arr, i, 0);
            int tri_width = pts[6];
            int* color = (int*)PyArray_GETPTR1(color_arr, i);

            int r = color[0];
            int g = color[1];
            int b = color[2];
            int a = (PyArray_DIM(color_arr, 1) == 4) ? color[3] : 255;

            if (tri_width > 1 && a != 255) {
                Py_DECREF(np_arr);
                Py_DECREF(color_arr);
                return PyErr_Format(PyExc_ValueError, "Alpha blending is not supported for width > 1");
            }

            Uint32 sdl_color = SDL_MapRGBA(surface->format, r, g, b, a);
            if (tri_width == 0) {
                // Draw filled triangle
                if (a == 255) {
                    draw_filled_triangle_wo_alpha(surface, pts, 3, sdl_color);
                } else {
                    draw_filled_triangle_w_alpha(surface, pts, 3, r, g, b, a);
                }
            } else {
                // Draw triangle border
                if (a == 255) {
                    draw_triangle_border_wo_alpha(surface, pts, sdl_color);
                } else {
                    draw_triangle_border_w_alpha(surface, pts, r, g, b, a);
                }
            }
        }

        Py_DECREF(color_arr);
    } else {
        Py_DECREF(np_arr);
        return PyErr_Format(PyExc_TypeError, "Color must be either a tuple or a numpy array");
    }

    Py_DECREF(np_arr);
    Py_RETURN_NONE;
}


// Function to calculate the cross product of two vectors
void cross_product(const float* v1, const float* v2, float* result) {
    result[0] = v1[1] * v2[2] - v1[2] * v2[1];
    result[1] = v1[2] * v2[0] - v1[0] * v2[2];
    result[2] = v1[0] * v2[1] - v1[1] * v2[0];
}

// Function to calculate the dot product of two vectors
float dot_product(const float* v1, const float* v2) {
    return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
}

// Function to subtract two vectors
void subtract_vectors(const float* v1, const float* v2, float* result) {
    result[0] = v1[0] - v2[0];
    result[1] = v1[1] - v2[1];
    result[2] = v1[2] - v2[2];
}

// Function to check if a triangle is facing the camera
bool is_triangle_facing_camera(const float* pts) {
    // Camera direction (assuming camera is at origin and looking along the z-axis)
    float camera_dir[3] = { 0.0f, 0.0f, 1.0f };

    float v0[3] = { pts[3 * 1 + 0] - pts[3 * 0 + 0], pts[3 * 1 + 1] - pts[3 * 0 + 1], pts[3 * 1 + 2] - pts[3 * 0 + 2] };
    float v1[3] = { pts[3 * 2 + 0] - pts[3 * 0 + 0], pts[3 * 2 + 1] - pts[3 * 0 + 1], pts[3 * 2 + 2] - pts[3 * 0 + 2] };
    float normal[3];
    cross_product(v0, v1, normal);

    return dot_product(normal, camera_dir) <= 0.0f;
}


static void draw_filled_triangle_w_zbuffer_wo_index_buffer(SDL_Surface* surface, float* pts, int n, Uint32 color, float* zbuffer, int surface_width, int surface_height) {
    if (!is_triangle_facing_camera(pts))
        return;

    const float bias = 0.0001f; // Small bias to handle depth precision issues

    // Simple scanline algorithm to fill the triangle with z-buffer
    int minY = (int)floorf(pts[1]), maxY = (int)ceilf(pts[1]);
    for (int i = 1; i < n; i++) {
        if ((int)floorf(pts[3 * i + 1]) < minY) minY = (int)floorf(pts[3 * i + 1]);
        if ((int)ceilf(pts[3 * i + 1]) > maxY) maxY = (int)ceilf(pts[3 * i + 1]);
    }

    minY = minY < 0 ? 0 : minY; // Clip minY to surface bounds
    maxY = maxY >= surface_height ? surface_height - 1 : maxY; // Clip maxY to surface bounds

    for (int y = minY; y <= maxY; y++) {
        int nodes = 0;
        float nodeX[6];
        float nodeZ[6];
        int j = 3 * (n - 1);
        for (int i = 0; i < n; i++) {
            if (((int)floorf(pts[3 * i + 1]) < y && (int)ceilf(pts[j + 1]) >= y) || ((int)floorf(pts[j + 1]) < y && (int)ceilf(pts[3 * i + 1]) >= y)) {
                float t = (float)(y - pts[3 * i + 1]) / (pts[j + 1] - pts[3 * i + 1]);
                nodeX[nodes] = pts[3 * i] + t * (pts[j] - pts[3 * i]);
                nodeZ[nodes++] = pts[3 * i + 2] + t * (pts[j + 2] - pts[3 * i + 2]);
            }
            j = 3 * i;
        }

        for (int i = 0; i < nodes - 1; i += 2) {
            if (nodeX[i] > nodeX[i + 1]) {
                float tempX = nodeX[i];
                nodeX[i] = nodeX[i + 1];
                nodeX[i + 1] = tempX;
                float tempZ = nodeZ[i];
                nodeZ[i] = nodeZ[i + 1];
                nodeZ[i + 1] = tempZ;
            }
            for (int x = (int)floorf(nodeX[i]); x <= (int)ceilf(nodeX[i + 1]); x++) { // Include the ending pixel
                if (x >= 0 && x < surface_width && y >= 0 && y < surface_height) { // Ensure x and y are within bounds
                    float t = (float)(x - nodeX[i]) / (nodeX[i + 1] - nodeX[i]);
                    float z = nodeZ[i] + t * (nodeZ[i + 1] - nodeZ[i]);
                    int index = y * surface_width + x;
                    if (z - bias < zbuffer[index]) { // Apply bias during the depth comparison
                        zbuffer[index] = z;
                        Uint32* bufp = (Uint32*)((Uint8*)surface->pixels + y * surface->pitch + x * surface->format->BytesPerPixel);
                        *bufp = color;
                    }
                }
            }
        }
    }
}

static void draw_filled_triangle_w_zbuffer_w_index_buffer(SDL_Surface* surface, float* pts, int n, Uint32 color, float* zbuffer, int surface_width, int surface_height, int* index_buffer, int triangle_index) {
    if (!is_triangle_facing_camera(pts))
        return;

    const float bias = 0.0001f; // Small bias to handle depth precision issues

    // Simple scanline algorithm to fill the triangle with z-buffer
    int minY = (int)floorf(pts[1]), maxY = (int)ceilf(pts[1]);
    for (int i = 1; i < n; i++) {
        if ((int)floorf(pts[3 * i + 1]) < minY) minY = (int)floorf(pts[3 * i + 1]);
        if ((int)ceilf(pts[3 * i + 1]) > maxY) maxY = (int)ceilf(pts[3 * i + 1]);
    }

    minY = minY < 0 ? 0 : minY; // Clip minY to surface bounds
    maxY = maxY >= surface_height ? surface_height - 1 : maxY; // Clip maxY to surface bounds

    for (int y = minY; y <= maxY; y++) {
        int nodes = 0;
        float nodeX[6];
        float nodeZ[6];
        int j = 3 * (n - 1);
        for (int i = 0; i < n; i++) {
            if (((int)floorf(pts[3 * i + 1]) < y && (int)ceilf(pts[j + 1]) >= y) || ((int)floorf(pts[j + 1]) < y && (int)ceilf(pts[3 * i + 1]) >= y)) {
                float t = (float)(y - pts[3 * i + 1]) / (pts[j + 1] - pts[3 * i + 1]);
                nodeX[nodes] = pts[3 * i] + t * (pts[j] - pts[3 * i]);
                nodeZ[nodes++] = pts[3 * i + 2] + t * (pts[j + 2] - pts[3 * i + 2]);
            }
            j = 3 * i;
        }

        for (int i = 0; i < nodes - 1; i += 2) {
            if (nodeX[i] > nodeX[i + 1]) {
                float tempX = nodeX[i];
                nodeX[i] = nodeX[i + 1];
                nodeX[i + 1] = tempX;
                float tempZ = nodeZ[i];
                nodeZ[i] = nodeZ[i + 1];
                nodeZ[i + 1] = tempZ;
            }
            for (int x = (int)floorf(nodeX[i]); x <= (int)ceilf(nodeX[i + 1]); x++) { // Include the ending pixel
                if (x >= 0 && x < surface_width && y >= 0 && y < surface_height) { // Ensure x and y are within bounds
                    float t = (float)(x - nodeX[i]) / (nodeX[i + 1] - nodeX[i]);
                    float z = nodeZ[i] + t * (nodeZ[i + 1] - nodeZ[i]);
                    int index = y * surface_width + x;
                    if (z - bias < zbuffer[index]) { // Apply bias during the depth comparison
                        zbuffer[index] = z;
                        index_buffer[index] = triangle_index;
                        Uint32* bufp = (Uint32*)((Uint8*)surface->pixels + y * surface->pitch + x * surface->format->BytesPerPixel);
                        *bufp = color;
                    }
                }
            }
        }
    }
}



static PyObject* draw_triangles_w_zbuffer(PyObject* self, PyObject* args) {
    PyObject* surface_ptr;
    PyObject* np_array;
    PyObject* color_array;
    PyObject* zbuffer_obj;
    PyObject* index_buffer_obj = Py_None;  // Default to None if not provided

    if (!PyArg_ParseTuple(args, "OOOO|O", &surface_ptr, &np_array, &color_array, &zbuffer_obj, &index_buffer_obj))
        return NULL;

    SDL_Surface* surface = PyCapsule_GetPointer(surface_ptr, "SDL_Surface");
    if (!surface) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid SDL_Surface pointer.");
        return NULL;
    }

    PyArrayObject* np_arr = (PyArrayObject*)PyArray_FROM_OTF(np_array, NPY_FLOAT32, NPY_ARRAY_IN_ARRAY);
    if (np_arr == NULL)
        return NULL;

    int width = PyArray_DIM(np_arr, 1);
    if (width != 9) {  // Each triangle must have three 3D coordinates, thus a Nx9 numpy array
        Py_DECREF(np_arr);
        return PyErr_Format(PyExc_ValueError, "Each triangle must have three 3D coordinates, thus a Nx9 numpy array");
    }

    int len = PyArray_DIM(np_arr, 0);

    PyArrayObject* zbuffer_arr = (PyArrayObject*)PyArray_FROM_OTF(zbuffer_obj, NPY_FLOAT32, NPY_ARRAY_IN_ARRAY);
    if (zbuffer_arr == NULL) {
        Py_DECREF(np_arr);
        return NULL;
    }

    if (PyArray_DIM(zbuffer_arr, 0) != surface->h || PyArray_DIM(zbuffer_arr, 1) != surface->w) {
        Py_DECREF(np_arr);
        Py_DECREF(zbuffer_arr);
        return PyErr_Format(PyExc_ValueError, "Z-buffer must have the same dimensions as the surface");
    }

    float* zbuffer = (float*)PyArray_DATA(zbuffer_arr);

    int* index_buffer = NULL;
    if (index_buffer_obj != Py_None) {
        PyArrayObject* index_buffer_arr = (PyArrayObject*)PyArray_FROM_OTF(index_buffer_obj, NPY_INT, NPY_ARRAY_IN_ARRAY);
        if (index_buffer_arr == NULL) {
            Py_DECREF(np_arr);
            Py_DECREF(zbuffer_arr);
            return NULL;
        }

        if (PyArray_DIM(index_buffer_arr, 0) != surface->h || PyArray_DIM(index_buffer_arr, 1) != surface->w) {
            Py_DECREF(np_arr);
            Py_DECREF(zbuffer_arr);
            Py_DECREF(index_buffer_arr);
            return PyErr_Format(PyExc_ValueError, "Triangle buffer must have the same dimensions as the surface and z-buffer");
        }

        index_buffer = (int*)PyArray_DATA(index_buffer_arr);
        Py_DECREF(index_buffer_arr);
    }

    if (PyArray_Check(color_array)) {
        // Array of colors mode
        PyArrayObject* color_arr = (PyArrayObject*)PyArray_FROM_OTF(color_array, NPY_INT, NPY_ARRAY_IN_ARRAY);
        if (color_arr == NULL) {
            Py_DECREF(np_arr);
            Py_DECREF(zbuffer_arr);
            return NULL;
        }

        int color_len = PyArray_DIM(color_arr, 0);
        if (color_len != len) {
            Py_DECREF(np_arr);
            Py_DECREF(color_arr);
            Py_DECREF(zbuffer_arr);
            return PyErr_Format(PyExc_ValueError, "The color array length must match the number of triangles");
        }

        for (int i = 0; i < len; i++) {
            float* pts = (float*)PyArray_GETPTR2(np_arr, i, 0);
            int* color = (int*)PyArray_GETPTR1(color_arr, i);

            int r = color[0];
            int g = color[1];
            int b = color[2];

            Uint32 sdl_color = SDL_MapRGB(surface->format, r, g, b);
            if (index_buffer)
                draw_filled_triangle_w_zbuffer_w_index_buffer(surface, pts, 3, sdl_color, zbuffer, surface->w, surface->h, index_buffer, i);
            else
                draw_filled_triangle_w_zbuffer_wo_index_buffer(surface, pts, 3, sdl_color, zbuffer, surface->w, surface->h);
        }

        Py_DECREF(color_arr);
    } else {
        Py_DECREF(np_arr);
        Py_DECREF(zbuffer_arr);
        return PyErr_Format(PyExc_TypeError, "Color must be a numpy array");
    }

    Py_DECREF(np_arr);
    Py_DECREF(zbuffer_arr);
    Py_RETURN_NONE;
}


void draw_filled_circle(SDL_Surface* surface, int cx, int cy, int radius, Uint32 color) {
    int x = radius;
    int y = 0;
    int radiusError = 1 - x;

    while (x >= y) {
        for (int i = cx - x; i <= cx + x; i++) {
            draw_pixel_wo_alpha(surface, i, cy + y, color);
            draw_pixel_wo_alpha(surface, i, cy - y, color);
        }
        for (int i = cx - y; i <= cx + y; i++) {
            draw_pixel_wo_alpha(surface, i, cy + x, color);
            draw_pixel_wo_alpha(surface, i, cy - x, color);
        }
        y++;
        if (radiusError < 0) {
            radiusError += 2 * y + 1;
        } else {
            x--;
            radiusError += 2 * (y - x + 1);
        }
    }
}

void draw_filled_circle_w_alpha(SDL_Surface* surface, int cx, int cy, int radius, int r, int g, int b, int a) {
    int x = radius;
    int y = 0;
    int radiusError = 1 - x;

    while (x >= y) {
        for (int i = cx - x; i <= cx + x; i++) {
            draw_pixel_w_alpha(surface, i, cy + y, r, g, b, a);
            draw_pixel_w_alpha(surface, i, cy - y, r, g, b, a);
        }
        for (int i = cx - y; i <= cx + y; i++) {
            draw_pixel_w_alpha(surface, i, cy + x, r, g, b, a);
            draw_pixel_w_alpha(surface, i, cy - x, r, g, b, a);
        }
        y++;
        if (radiusError < 0) {
            radiusError += 2 * y + 1;
        } else {
            x--;
            radiusError += 2 * (y - x + 1);
        }
    }
}

void draw_circle_border(SDL_Surface* surface, int cx, int cy, int radius, Uint32 color, int border_width) {
    for (int w = 0; w < border_width; w++) {
        int x = radius - w;
        int y = 0;
        int radiusError = 1 - x;

        while (x >= y) {
            draw_pixel_wo_alpha(surface, cx + x, cy + y, color);
            draw_pixel_wo_alpha(surface, cx - x, cy + y, color);
            draw_pixel_wo_alpha(surface, cx + x, cy - y, color);
            draw_pixel_wo_alpha(surface, cx - x, cy - y, color);
            draw_pixel_wo_alpha(surface, cx + y, cy + x, color);
            draw_pixel_wo_alpha(surface, cx - y, cy + x, color);
            draw_pixel_wo_alpha(surface, cx + y, cy - x, color);
            draw_pixel_wo_alpha(surface, cx - y, cy - x, color);

            y++;
            if (radiusError < 0) {
                radiusError += 2 * y + 1;
            } else {
                x--;
                radiusError += 2 * (y - x + 1);
            }
        }
    }
}

void draw_circle_border_w_alpha(SDL_Surface* surface, int cx, int cy, int radius, int r, int g, int b, int a, int border_width) {
    for (int w = 0; w < border_width; w++) {
        int x = radius - w;
        int y = 0;
        int radiusError = 1 - x;

        while (x >= y) {
            draw_pixel_w_alpha(surface, cx + x, cy + y, r, g, b, a);
            draw_pixel_w_alpha(surface, cx - x, cy + y, r, g, b, a);
            draw_pixel_w_alpha(surface, cx + x, cy - y, r, g, b, a);
            draw_pixel_w_alpha(surface, cx - x, cy - y, r, g, b, a);
            draw_pixel_w_alpha(surface, cx + y, cy + x, r, g, b, a);
            draw_pixel_w_alpha(surface, cx - y, cy + x, r, g, b, a);
            draw_pixel_w_alpha(surface, cx + y, cy - x, r, g, b, a);
            draw_pixel_w_alpha(surface, cx - y, cy - x, r, g, b, a);

            y++;
            if (radiusError < 0) {
                radiusError += 2 * y + 1;
            } else {
                x--;
                radiusError += 2 * (y - x + 1);
            }
        }
    }
}

static PyObject* draw_circles(PyObject* self, PyObject* args) {
    PyObject* surface_ptr;
    PyObject* np_array;
    PyObject* color_tuple;

    if (!PyArg_ParseTuple(args, "OOO", &surface_ptr, &np_array, &color_tuple))
        return NULL;

    int r, g, b, a;
    if (!parse_color(color_tuple, &r, &g, &b, &a)) {
        return NULL;  // Error has been set inside parse_color
    }

    SDL_Surface* surface = PyCapsule_GetPointer(surface_ptr, "SDL_Surface");
    if (!surface) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid SDL_Surface pointer.");
        return NULL;
    }

    PyArrayObject* np_arr = (PyArrayObject*)PyArray_FROM_OTF(np_array, NPY_INT, NPY_ARRAY_IN_ARRAY);
    if (np_arr == NULL)
        return NULL;

    int len = PyArray_DIM(np_arr, 0);

    for (int i = 0; i < len; ++i) {
        int* circle_data = (int*)PyArray_GETPTR2(np_arr, i, 0);
        int cx = circle_data[0];
        int cy = circle_data[1];
        int radius = circle_data[2];
        int border_width = circle_data[3];

        if (border_width == 0) {
            if (a == 255) {
                Uint32 color = SDL_MapRGBA(surface->format, r, g, b, a);
                draw_filled_circle(surface, cx, cy, radius, color);
            } else {
                draw_filled_circle_w_alpha(surface, cx, cy, radius, r, g, b, a);
            }
        } else {
            if (a == 255) {
                Uint32 color = SDL_MapRGBA(surface->format, r, g, b, a);
                draw_circle_border(surface, cx, cy, radius, color, border_width);
            } else {
                draw_circle_border_w_alpha(surface, cx, cy, radius, r, g, b, a, border_width);
            }
        }
    }

    Py_DECREF(np_arr);
    Py_RETURN_NONE;
}

// Function to check if any pixels of a surface have alpha != 255
static bool has_transparent_pixels(SDL_Surface* surface) {
    Uint32* pixels = (Uint32*)surface->pixels;
    int width = surface->w;
    int height = surface->h;

    // Get the format of the surface
    SDL_PixelFormat* fmt = surface->format;
    Uint32 alpha_mask = fmt->Amask;
    Uint8 alpha_shift = fmt->Ashift;
    Uint8 alpha_loss = fmt->Aloss;

    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            Uint32 pixel = pixels[y * surface->pitch / 4 + x];
            Uint8 alpha = (Uint8)((pixel & alpha_mask) >> alpha_shift);
            alpha = alpha << alpha_loss; // Correct for the loss of precision
            if (alpha != 255) {
                return true;
            }
        }
    }
    return false;
}


// Structure to manage the boundary list
typedef struct {
    int (*boundaries)[4];
    int size;
    int capacity;
} BoundaryList;

// Function to initialize the BoundaryList
static bool init_boundary_list(BoundaryList* bl, int initial_capacity) {
    bl->boundaries = (int(*)[4])malloc(initial_capacity * sizeof(int[4]));
    if (!bl->boundaries) return false;
    bl->size = 0;
    bl->capacity = initial_capacity;
    return true;
}

// Function to append a boundary to the BoundaryList
static bool boundary_list_append(BoundaryList* bl, int x_min, int y_min, int x_max, int y_max) {
    if (bl->size >= bl->capacity) {
        int new_capacity = bl->capacity * 2;
        int (*new_boundaries)[4] = (int(*)[4])realloc(bl->boundaries, new_capacity * sizeof(int[4]));
        if (!new_boundaries) return false;
        bl->boundaries = new_boundaries;
        bl->capacity = new_capacity;
    }
    bl->boundaries[bl->size][0] = x_min;
    bl->boundaries[bl->size][1] = y_min;
    bl->boundaries[bl->size][2] = x_max;
    bl->boundaries[bl->size][3] = y_max;
    bl->size++;
    return true;
}

// Function to free the BoundaryList
static void free_boundary_list(BoundaryList* bl) {
    free(bl->boundaries);
    bl->boundaries = NULL;
    bl->size = 0;
    bl->capacity = 0;
}

// Helper function to get the boundaries of a shape
static bool get_shape_boundaries(PyObject* shape, int x_offset, int y_offset, int* x_min, int* y_min, int* x_max, int* y_max) {
    const char* shape_type = PyUnicode_AsUTF8(PyTuple_GetItem(shape, 0));

    if (strcmp(shape_type, "clear") == 0) {
        *x_min = 0;
        *y_min = 0;
        *x_max = INT_MAX;
        *y_max = INT_MAX;
        return true;
    } else if (strcmp(shape_type, "rectangle") == 0) {
        int x = PyLong_AsLong(PyTuple_GetItem(shape, 1));
        int y = PyLong_AsLong(PyTuple_GetItem(shape, 2));
        int width = PyLong_AsLong(PyTuple_GetItem(shape, 3));
        int height = PyLong_AsLong(PyTuple_GetItem(shape, 4));
        *x_min = x + x_offset;
        *y_min = y + y_offset;
        *x_max = x + width + x_offset;
        *y_max = y + height + y_offset;
        return true;
    } else if (strcmp(shape_type, "blit") == 0) {
        PyObject* src_surface_ptr;
        int x, y;
        int has_transparency = -1; // -1 indicates not-present

        if (!PyArg_ParseTuple(shape, "sOii|p", &shape_type, &src_surface_ptr, &x, &y, &has_transparency)) {
            PyErr_SetString(PyExc_TypeError, "Blit tuple must be of the form (\"blit\", src_surface, x, y, [has_transparency]).");
            return -1;
        }
    
        SDL_Surface* src_surface = PyCapsule_GetPointer(src_surface_ptr, "SDL_Surface");
        if (!src_surface)
            return false;
        if (has_transparency == -1) {
            has_transparency = has_transparent_pixels(src_surface);
        }
        if (has_transparency)
            return false;
        *x_min = x + x_offset;
        *y_min = y + y_offset;
        *x_max = x + src_surface->w + x_offset;
        *y_max = y + src_surface->h + y_offset;
        return true;
    }

    return false;
}

// Helper function to check if a shape is inside the boundaries of a covering shape, considering the offset
static bool is_inside(PyObject* shape, int x_min, int y_min, int x_max, int y_max, int x_offset, int y_offset) {
    const char* shape_type = PyUnicode_AsUTF8(PyTuple_GetItem(shape, 0));

    if (strcmp(shape_type, "line") == 0) {
        int x0 = PyLong_AsLong(PyTuple_GetItem(shape, 1));
        int y0 = PyLong_AsLong(PyTuple_GetItem(shape, 2));
        int x1 = PyLong_AsLong(PyTuple_GetItem(shape, 3));
        int y1 = PyLong_AsLong(PyTuple_GetItem(shape, 4));
        return (x0 + x_offset >= x_min && y0 + y_offset >= y_min && x0 + x_offset <= x_max && y0 + y_offset <= y_max &&
                x1 + x_offset >= x_min && y1 + y_offset >= y_min && x1 + x_offset <= x_max && y1 + y_offset <= y_max);
    } else if (strcmp(shape_type, "triangle") == 0) {
        int x0 = PyLong_AsLong(PyTuple_GetItem(shape, 1));
        int y0 = PyLong_AsLong(PyTuple_GetItem(shape, 2));
        int x1 = PyLong_AsLong(PyTuple_GetItem(shape, 3));
        int y1 = PyLong_AsLong(PyTuple_GetItem(shape, 4));
        int x2 = PyLong_AsLong(PyTuple_GetItem(shape, 5));
        int y2 = PyLong_AsLong(PyTuple_GetItem(shape, 6));
        return (x0 + x_offset >= x_min && y0 + y_offset >= y_min && x0 + x_offset <= x_max && y0 + y_offset <= y_max &&
                x1 + x_offset >= x_min && y1 + y_offset >= y_min && x1 + x_offset <= x_max && y1 + y_offset <= y_max &&
                x2 + x_offset >= x_min && y2 + y_offset >= y_min && x2 + x_offset <= x_max && y2 + y_offset <= y_max);
    } else if (strcmp(shape_type, "circle") == 0) {
        int cx = PyLong_AsLong(PyTuple_GetItem(shape, 1));
        int cy = PyLong_AsLong(PyTuple_GetItem(shape, 2));
        int radius = PyLong_AsLong(PyTuple_GetItem(shape, 3));
        return (cx - radius + x_offset >= x_min && cy - radius + y_offset >= y_min &&
                cx + radius + x_offset <= x_max && cy + radius + y_offset <= y_max);
    } else if (strcmp(shape_type, "rectangle") == 0) {
        int x = PyLong_AsLong(PyTuple_GetItem(shape, 1));
        int y = PyLong_AsLong(PyTuple_GetItem(shape, 2));
        int width = PyLong_AsLong(PyTuple_GetItem(shape, 3));
        int height = PyLong_AsLong(PyTuple_GetItem(shape, 4));
        return (x + x_offset >= x_min && y + y_offset >= y_min && (x + width + x_offset) <= x_max && (y + height + y_offset) <= y_max);
    } else if (strcmp(shape_type, "blit") == 0) {
        int x = PyLong_AsLong(PyTuple_GetItem(shape, 2));
        int y = PyLong_AsLong(PyTuple_GetItem(shape, 3));
        SDL_Surface* src_surface = PyCapsule_GetPointer(PyTuple_GetItem(shape, 1), "SDL_Surface");
        if (!src_surface) return false;
        return (x + x_offset >= x_min && y + y_offset >= y_min && (x + src_surface->w + x_offset) <= x_max && (y + src_surface->h + y_offset) <= y_max);
    }

    return false;
}

// Simplify the draw list by removing covered shapes
static PyObject* simplify_draw_list(PyObject* draw_list, int x_offset, int y_offset, BoundaryList* bl) {
    Py_ssize_t list_len = PyList_Size(draw_list);
    PyObject* simplified_list = PyList_New(0);

    for (Py_ssize_t i = list_len - 1; i >= 0; --i) {
        PyObject* shape = PyList_GetItem(draw_list, i);
        bool covered = false;

        for (int j = 0; j < bl->size; ++j) {
            if (is_inside(shape, bl->boundaries[j][0], bl->boundaries[j][1], bl->boundaries[j][2], bl->boundaries[j][3], x_offset, y_offset)) {
                covered = true;
                break;
            }
        }

        if (!covered) {
            PyList_Append(simplified_list, shape);

            int x_min = 0, y_min = 0, x_max = 0, y_max = 0;
            if (get_shape_boundaries(shape, x_offset, y_offset, &x_min, &y_min, &x_max, &y_max)) {
                boundary_list_append(bl, x_min, y_min, x_max, y_max);
            }

            const char* shape_type = PyUnicode_AsUTF8(PyTuple_GetItem(shape, 0));
            if (strcmp(shape_type, "draw") == 0) {
                int nested_offset_x, nested_offset_y;
                PyObject* nested_draw_list;
                if (!PyArg_ParseTuple(shape, "sO(ii)", &shape_type, &nested_draw_list, &nested_offset_x, &nested_offset_y)) {
                    PyErr_SetString(PyExc_TypeError, "Draw tuple must be of the form (\"draw\", drawlist, (offset_x, offset_y)).");
                    return NULL;
                }
                if (!PyList_Check(draw_list)) {
                    PyErr_SetString(PyExc_TypeError, "Expected a list of tuples.");
                    return NULL;
                }

                nested_offset_x += x_offset;
                nested_offset_y += y_offset;
                simplify_draw_list(nested_draw_list, nested_offset_x, nested_offset_y, bl);
            }
        }
    }

    // Reverse the list to maintain the original order
    PyObject* reversed_list = PyList_New(0);
    for (Py_ssize_t i = PyList_Size(simplified_list) - 1; i >= 0; --i) {
        PyObject* item = PyList_GetItem(simplified_list, i);
        PyList_Append(reversed_list, item);
    }

    return reversed_list;
}

// Python-callable function
static PyObject* py_simplify_draw_list(PyObject* self, PyObject* args) {
    PyObject* draw_list;

    if (!PyArg_ParseTuple(args, "O", &draw_list)) {
        return NULL;
    }

    if (!PyList_Check(draw_list)) {
        PyErr_SetString(PyExc_TypeError, "Expected a list of shapes.");
        return NULL;
    }

    BoundaryList bl;
    if (!init_boundary_list(&bl, 16)) { // Initial capacity of 16
        PyErr_SetString(PyExc_RuntimeError, "Failed to initialize boundary list.");
        return NULL;
    }

    PyObject* result = simplify_draw_list(draw_list, 0, 0, &bl);
    free_boundary_list(&bl);
    return result;
}


static void blit_and_scale_w_alpha(SDL_Surface* dst_surface, SDL_Surface* src_surface, int dst_x, int dst_y, float scale_x, float scale_y, Uint8 alpha) {
    if (!dst_surface || !src_surface) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid SDL_Surface pointer.");
        return;
    }

    int scaled_width = (int)(src_surface->w * scale_x);
    int scaled_height = (int)(src_surface->h * scale_y);

    // Iterate through the destination surface pixels within the scaled region
    for (int y = 0; y < scaled_height; ++y) {
        int dst_y_pos = dst_y + y;

        // Stop if the y coordinate is outside the destination surface height
        if (dst_y_pos >= dst_surface->h) break;
        if (dst_y_pos < 0) continue;

        for (int x = 0; x < scaled_width; ++x) {
            int dst_x_pos = dst_x + x;

            // Skip the rest of the line if the x coordinate is outside the destination surface width
            if (dst_x_pos >= dst_surface->w) break;
            if (dst_x_pos < 0) continue;

            // Calculate the corresponding source pixel
            int src_x = (int)(x / scale_x);
            int src_y = (int)(y / scale_y);

            // Ensure the source pixel is within the bounds of the source surface
            if (src_x >= 0 && src_x < src_surface->w && src_y >= 0 && src_y < src_surface->h) {
                Uint32* src_pixel = (Uint32*)((Uint8*)src_surface->pixels + src_y * src_surface->pitch + src_x * src_surface->format->BytesPerPixel);

                // Extract the source color components
                Uint8 src_r, src_g, src_b, src_a;
                SDL_GetRGBA(*src_pixel, src_surface->format, &src_r, &src_g, &src_b, &src_a);
                src_a = src_a * alpha / 255;
                // Draw the pixel with alpha blending
                draw_pixel_w_alpha(dst_surface, dst_x_pos, dst_y_pos, src_r, src_g, src_b, src_a);
            }
        }
    }
}


static int draw_shape(SDL_Surface* surface, int origin_x, int origin_y, PyObject* item, Uint8 alpha) {
    if (!PyTuple_Check(item)) {
        PyErr_SetString(PyExc_TypeError, "Each item in the list must be a tuple.");
        return -1;
    }

    PyObject* shape_type_obj = PyTuple_GetItem(item, 0);
    if (!PyUnicode_Check(shape_type_obj)) {
        PyErr_SetString(PyExc_TypeError, "The first element of each tuple must be a string.");
        return -1;
    }

    const char* shape_type = PyUnicode_AsUTF8(shape_type_obj);
    if (shape_type == NULL) {
        PyErr_SetString(PyExc_RuntimeError, "Failed to get string value.");
        return -1;
    }

    if (strcmp(shape_type, "line") == 0) {
        int x0, y0, x1, y1;
        PyObject* color_obj;
        if (!PyArg_ParseTuple(item, "siiiiO", &shape_type, &x0, &y0, &x1, &y1, &color_obj)) {
            PyErr_SetString(PyExc_TypeError, "Line tuple must be of the form (\"line\", x0, y0, x1, y1, color_tuple).");
            return -1;
        }

        x0 += origin_x; y0 += origin_y;
        x1 += origin_x; y1 += origin_y;

        int r, g, b, a;
        if (!parse_color(color_obj, &r, &g, &b, &a)) {
            return -1;  // Error has been set inside parse_color
        }

        if ((a == 255) && (alpha == 255)) {
            Uint32 color = SDL_MapRGBA(surface->format, r, g, b, a);
            draw_line_wo_alpha(surface, x0, y0, x1, y1, color);
        } else {
            draw_line_w_alpha(surface, x0, y0, x1, y1, r, g, b, (a * alpha) / 255);
        }
    } else if (strcmp(shape_type, "clear") == 0) {
        PyObject* color_obj;
        if (!PyArg_ParseTuple(item, "sO", &shape_type, &color_obj)) {
            PyErr_SetString(PyExc_TypeError, "Clear tuple must be of the form (\"clear\", color_tuple).");
            return -1;
        }

        int r, g, b, a;
        if (!parse_color(color_obj, &r, &g, &b, &a)) {
            return -1;  // Error has been set inside parse_color
        }

        SDL_FillRect(surface, NULL, SDL_MapRGBA(surface->format, r, g, b, (a * alpha) / 255));
    } else if (strcmp(shape_type, "rectangle") == 0) {
        int x, y, width, height, border_width;
        PyObject* color_obj;
        if (!PyArg_ParseTuple(item, "siiiiiO", &shape_type, &x, &y, &width, &height, &border_width, &color_obj)) {
            PyErr_SetString(PyExc_TypeError, "Rectangle tuple must be of the form (\"rectangle\", x, y, width, height, border_width, color_tuple).");
            return -1;
        }

        x += origin_x; y += origin_y;

        int r, g, b, a;
        if (!parse_color(color_obj, &r, &g, &b, &a)) {
            return -1;  // Error has been set inside parse_color
        }

        if (border_width <= 0) {
            if ((a == 255) && (alpha == 255)) {
                Uint32 color = SDL_MapRGBA(surface->format, r, g, b, a);
                fill_rect_wo_alpha(surface, x, y, width, height, color);
            } else {
                fill_rect_w_alpha(surface, x, y, width, height, r, g, b, a * alpha / 255);
            }
        } else {
            if ((a == 255) && (alpha == 255)) {
                Uint32 color = SDL_MapRGBA(surface->format, r, g, b, a);
                draw_border_wo_alpha(surface, x, y, width, height, color, border_width);
            } else {
                draw_border_w_alpha(surface, x, y, width, height, r, g, b, a * alpha / 255, border_width);
            }
        }
    } else if (strcmp(shape_type, "triangle") == 0) {
        int x0, y0, x1, y1, x2, y2, border_width;
        PyObject* color_obj;
        if (!PyArg_ParseTuple(item, "siiiiiiiO", &shape_type, &x0, &y0, &x1, &y1, &x2, &y2, &border_width, &color_obj)) {
            PyErr_SetString(PyExc_TypeError, "Triangle tuple must be of the form (\"triangle\", x0, y0, x1, y1, x2, y2, border_width, color_tuple).");
            return -1;
        }
        x0 += origin_x; y0 += origin_y;
        x1 += origin_x; y1 += origin_y;
        x2 += origin_x; y2 += origin_y;

        int r, g, b, a;
        if (!parse_color(color_obj, &r, &g, &b, &a)) {
            return -1;  // Error has been set inside parse_color
        }

        if (border_width <= 0) {
            if ((a == 255) && (alpha == 255)) {
                Uint32 color = SDL_MapRGBA(surface->format, r, g, b, a);
                draw_filled_triangle_wo_alpha(surface, (int[]){x0, y0, x1, y1, x2, y2}, 3, color);
            } else {
                draw_filled_triangle_w_alpha(surface, (int[]){x0, y0, x1, y1, x2, y2}, 3, r, g, b, a * alpha / 255);
            }
        } else {
            if ((a == 255) && (alpha == 255)) {
                Uint32 color = SDL_MapRGBA(surface->format, r, g, b, a);
                draw_triangle_border_wo_alpha(surface, (int[]){x0, y0, x1, y1, x2, y2}, color);
            } else {
                draw_triangle_border_w_alpha(surface, (int[]){x0, y0, x1, y1, x2, y2}, r, g, b, a * alpha / 255);
            }
        }
    } else if (strcmp(shape_type, "draw") == 0) {
        int offset_x, offset_y, nested_alpha;
        PyObject* draw_list;
        PyObject* rect = NULL;

        if (!PyArg_ParseTuple(item, "sO(ii)i|O", &shape_type, &draw_list, &offset_x, &offset_y, &nested_alpha, &rect)) {
            PyErr_SetString(PyExc_TypeError, "Draw tuple must be of the form (\"draw\", drawlist, (offset_x, offset_y), alpha, [rect]).");
            return -1;
        }
        if (!PyList_Check(draw_list)) {
            PyErr_SetString(PyExc_TypeError, "Expected a list of tuples.");
            return -1;
        }

        SDL_Surface* target_surface = surface;

        if (rect && PyTuple_Check(rect) && PyTuple_Size(rect) == 4) {
            int rect_x = PyLong_AsLong(PyTuple_GetItem(rect, 0));
            int rect_y = PyLong_AsLong(PyTuple_GetItem(rect, 1));
            int rect_w = PyLong_AsLong(PyTuple_GetItem(rect, 2));
            int rect_h = PyLong_AsLong(PyTuple_GetItem(rect, 3));

            // Ensure the rectangle does not exceed the parent surface boundaries
            if (rect_x < 0) { origin_x += rect_x; rect_w += rect_x; rect_x = 0; }
            if (rect_y < 0) { origin_y += rect_y; rect_h += rect_y; rect_y = 0; }
            if (rect_x + rect_w > surface->w) rect_w = surface->w - rect_x;
            if (rect_y + rect_h > surface->h) rect_h = surface->h - rect_y;
            if ((rect_w <= 0) || (rect_h <= 0))
                return 0; // Surface is entierly outside parent; nothing to do.

            target_surface = SDL_CreateRGBSurfaceWithFormatFrom(
                (Uint8*)surface->pixels + rect_y * surface->pitch + rect_x * surface->format->BytesPerPixel,
                rect_w, rect_h, surface->format->BitsPerPixel, surface->pitch, surface->format->format);

            if (!target_surface) {
                PyErr_SetString(PyExc_RuntimeError, "Failed to create subsurface.");
                return -1;
            }
        }
        offset_x += origin_x;
        offset_y += origin_y;

        Py_ssize_t list_len = PyList_Size(draw_list);
        for (Py_ssize_t i = 0; i < list_len; ++i) {
            PyObject* item = PyList_GetItem(draw_list, i);
            if (draw_shape(target_surface, offset_x, offset_y, item, alpha * nested_alpha / 255) < 0) {
                if (target_surface != surface) {
                    SDL_FreeSurface(target_surface);
                }
                return -1;
            }
        }

        if (target_surface != surface) {
            SDL_FreeSurface(target_surface);
        }
    } else if (strcmp(shape_type, "blit") == 0) {
        PyObject* src_surface_ptr;
        int x, y, src_alpha;
        float scale_x, scale_y;
        int has_transparency = -1; // -1 indicates not-present

        if (!PyArg_ParseTuple(item, "sOiiffi|p", &shape_type, &src_surface_ptr, &x, &y, &scale_x, &scale_y, &src_alpha, &has_transparency)) {
            PyErr_SetString(PyExc_TypeError, "Blit tuple must be of the form (\"blit\", src_surface, x, y, scalex, scaley, alpha, [has_transparency]).");
            return -1;
        }
    
        SDL_Surface* src_surface = PyCapsule_GetPointer(src_surface_ptr, "SDL_Surface");
        if (!src_surface) {
            PyErr_SetString(PyExc_RuntimeError, "Invalid SDL_Surface pointer.");
            return -1;
        }

        x += origin_x; y += origin_y;

        src_alpha = src_alpha * alpha / 255;
        if ((scale_x != 1.0) || (scale_y != 1.0) || (src_alpha != 255)) {
            blit_and_scale_w_alpha(surface, src_surface, x, y, scale_x, scale_y, src_alpha);
        } else {
            SDL_Rect dest_rect;
            dest_rect.x = x;
            dest_rect.y = y;
            SDL_BlitSurface(src_surface, NULL, surface, &dest_rect);
        }
    } else if (strcmp(shape_type, "circle") == 0) {
        int cx, cy, radius, border_width;
        PyObject* color_obj;
        if (!PyArg_ParseTuple(item, "siiiiO", &shape_type, &cx, &cy, &radius, &border_width, &color_obj)) {
            PyErr_SetString(PyExc_TypeError, "Circle tuple must be of the form (\"circle\", cx, cy, radius, border_width, color_tuple).");
            return -1;
        }

        cx += origin_x; cy += origin_y;

        int r, g, b, a;
        if (!parse_color(color_obj, &r, &g, &b, &a)) {
            return -1;  // Error has been set inside parse_color
        }

        if (border_width == 0) {
            if ((a == 255) && (alpha == 255)) {
                Uint32 color = SDL_MapRGBA(surface->format, r, g, b, a);
                draw_filled_circle(surface, cx, cy, radius, color);
            } else {
                draw_filled_circle_w_alpha(surface, cx, cy, radius, r, g, b, a * alpha / 255);
            }
        } else {
            if ((a == 255) && (alpha == 255)) {
                Uint32 color = SDL_MapRGBA(surface->format, r, g, b, a);
                draw_circle_border(surface, cx, cy, radius, color, border_width);
            } else {
                draw_circle_border_w_alpha(surface, cx, cy, radius, r, g, b, a * alpha / 255, border_width);
            }
        }
    }
    return 0;
}


// Main draw function
static PyObject* draw(PyObject* self, PyObject* args, PyObject* kwargs) {
    PyObject* surface_ptr;
    PyObject* draw_list;
    PyObject* origin_obj = NULL;
    int alpha = 255;
    static char* kwlist[] = {"surface", "draw_list", "origin", "alpha", NULL};

    if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OO|Oi", kwlist, &surface_ptr, &draw_list, &origin_obj, &alpha))
        return NULL;

    int origin_x = 0, origin_y = 0;
    if (origin_obj) {
        if (!PyTuple_Check(origin_obj) || PyTuple_Size(origin_obj) != 2) {
            PyErr_SetString(PyExc_TypeError, "Origin must be a tuple of the form (x, y).");
            return NULL;
        }
        if (!PyArg_ParseTuple(origin_obj, "ii", &origin_x, &origin_y)) {
            PyErr_SetString(PyExc_TypeError, "Origin must be a tuple of two integers.");
            return NULL;
        }
    }

    SDL_Surface* surface = PyCapsule_GetPointer(surface_ptr, "SDL_Surface");
    if (!surface) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid SDL_Surface pointer.");
        return NULL;
    }

    if (!PyList_Check(draw_list)) {
        PyErr_SetString(PyExc_TypeError, "Expected a list of tuples.");
        return NULL;
    }

    Py_ssize_t list_len = PyList_Size(draw_list);
    for (Py_ssize_t i = 0; i < list_len; ++i) {
        PyObject* item = PyList_GetItem(draw_list, i);
        if (draw_shape(surface, origin_x, origin_y, item, alpha) < 0) {
            return NULL;
        }
    }

    Py_RETURN_NONE;
}

static void free_sdl_surface(PyObject* capsule) {
    SDL_Surface* surface = (SDL_Surface*)PyCapsule_GetPointer(capsule, "SDL_Surface");
    if (surface) {
        SDL_FreeSurface(surface);
    }
}


static PyObject* surface_subsurface(PyObject* self, PyObject* args) {
    PyObject* surface_ptr;
    int x, y, w, h;

    if (!PyArg_ParseTuple(args, "Oiiii", &surface_ptr, &x, &y, &w, &h)) {
        return NULL;
    }

    SDL_Surface* main_surface = (SDL_Surface*)PyCapsule_GetPointer(surface_ptr, "SDL_Surface");
    if (!main_surface) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid SDL_Surface pointer.");
        return NULL;
    }

    // Ensure the coordinates and dimensions are within the bounds of the main surface
    if (x < 0 || y < 0 || x + w > main_surface->w || y + h > main_surface->h) {
        PyErr_SetString(PyExc_ValueError, "Subsurface coordinates out of bounds.");
        return NULL;
    }

    // Calculate the pixel pointer for the subsurface
    Uint8* pixels = (Uint8*)main_surface->pixels + y * main_surface->pitch + x * main_surface->format->BytesPerPixel;

    // Create the subsurface with the correct pitch
    SDL_Surface* sub_surface = SDL_CreateRGBSurfaceFrom(
        pixels,
        w, h, main_surface->format->BitsPerPixel, main_surface->pitch,
        main_surface->format->Rmask, main_surface->format->Gmask, 
        main_surface->format->Bmask, main_surface->format->Amask
    );

    if (!sub_surface) {
        PyErr_SetString(PyExc_RuntimeError, SDL_GetError());
        return NULL;
    }

    // Create a capsule with a destructor to free the subsurface when done
    return PyCapsule_New(sub_surface, "SDL_Surface", free_sdl_surface);
}


static void font_destructor(PyObject* capsule) {
    TTF_Font* font = (TTF_Font*)PyCapsule_GetPointer(capsule, "TTF_Font");
    if (font) {
        TTF_CloseFont(font);
    }
}

static PyObject* font_load(PyObject* self, PyObject* args) {
    const char* font_path;
    int font_size;
    
    if (!PyArg_ParseTuple(args, "si", &font_path, &font_size)) {
        return NULL;
    }

    if (TTF_Init() == -1) {
        PyErr_SetString(PyExc_RuntimeError, TTF_GetError());
        return NULL;
    }

    TTF_Font* font = TTF_OpenFont(font_path, font_size);
    if (!font) {
        PyErr_SetString(PyExc_RuntimeError, TTF_GetError());
        return NULL;
    }

    return PyCapsule_New(font, "TTF_Font", font_destructor);
}

static PyObject* font_render(PyObject* self, PyObject* args) {
    PyObject* font_ptr;
    const char* text;
    PyObject* color_obj;
    int r, g, b, a;

    if (!PyArg_ParseTuple(args, "OsO", &font_ptr, &text, &color_obj)) {
        return NULL;
    }

    if (!parse_color(color_obj, &r, &g, &b, &a)) {
        return NULL;  // Error already set by parse_color
    }

    TTF_Font* font = PyCapsule_GetPointer(font_ptr, "TTF_Font");
    if (!font) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid TTF_Font pointer.");
        return NULL;
    }

    SDL_Color color = {r, g, b, a};
    SDL_Surface* text_surface = TTF_RenderText_Blended(font, text, color);
    if (!text_surface) {
        PyErr_SetString(PyExc_RuntimeError, TTF_GetError());
        return NULL;
    }

    return PyCapsule_New(text_surface, "SDL_Surface", free_sdl_surface);
}

static PyObject* font_text_size(PyObject* self, PyObject* args) {
    PyObject* font_ptr;
    const char* text;

    if (!PyArg_ParseTuple(args, "Os", &font_ptr, &text)) {
        return NULL;
    }

    TTF_Font* font = PyCapsule_GetPointer(font_ptr, "TTF_Font");
    if (!font) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid TTF_Font pointer.");
        return NULL;
    }

    int width, height;
    if (TTF_SizeText(font, text, &width, &height) != 0) {
        PyErr_SetString(PyExc_RuntimeError, TTF_GetError());
        return NULL;
    }

    return Py_BuildValue("(ii)", width, height);
}

static PyObject* font_info(PyObject* self, PyObject* args) {
    PyObject* font_ptr;

    if (!PyArg_ParseTuple(args, "O", &font_ptr)) {
        return NULL;
    }

    TTF_Font* font = PyCapsule_GetPointer(font_ptr, "TTF_Font");
    if (!font) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid TTF_Font pointer.");
        return NULL;
    }

    PyObject* font_info = PyDict_New();
    if (!font_info) {
        return NULL;
    }

    PyDict_SetItemString(font_info, "ascent", PyLong_FromLong(TTF_FontAscent(font)));
    PyDict_SetItemString(font_info, "descent", PyLong_FromLong(TTF_FontDescent(font)));
    PyDict_SetItemString(font_info, "height", PyLong_FromLong(TTF_FontHeight(font)));
    PyDict_SetItemString(font_info, "line_skip", PyLong_FromLong(TTF_FontLineSkip(font)));
    PyDict_SetItemString(font_info, "faces", PyLong_FromLong(TTF_FontFaces(font)));
    PyDict_SetItemString(font_info, "monospace", PyBool_FromLong(TTF_FontFaceIsFixedWidth(font)));

    const char* family_name = TTF_FontFaceFamilyName(font);
    if (family_name) {
        PyDict_SetItemString(font_info, "family_name", PyUnicode_FromString(family_name));
    } else {
        PyDict_SetItemString(font_info, "family_name", Py_None);
        Py_INCREF(Py_None);
    }

    const char* style_name = TTF_FontFaceStyleName(font);
    if (style_name) {
        PyDict_SetItemString(font_info, "style_name", PyUnicode_FromString(style_name));
    } else {
        PyDict_SetItemString(font_info, "style_name", Py_None);
        Py_INCREF(Py_None);
    }

    // Additional attributes
    PyDict_SetItemString(font_info, "style", PyLong_FromLong(TTF_GetFontStyle(font)));
    PyDict_SetItemString(font_info, "utline", PyLong_FromLong(TTF_GetFontOutline(font)));
    PyDict_SetItemString(font_info, "hinting", PyLong_FromLong(TTF_GetFontHinting(font)));
    PyDict_SetItemString(font_info, "kerning", PyBool_FromLong(TTF_GetFontKerning(font)));

    // Font size is approximated by the height as TTF does not directly provide a specific size
    PyDict_SetItemString(font_info, "height", PyLong_FromLong(TTF_FontHeight(font)));

    return font_info;
}




static PyObject* surface_blit(PyObject* self, PyObject* args, PyObject* keywds) {
    PyObject* dst_surface_ptr;
    PyObject* src_surface_ptr;
    int x, y, alpha;
    float scale = 1.0; // Default scale is 1.0 (no scaling)
    static char *kwlist[] = {"dst", "src", "pos", "scale", "alpha", NULL};
    // Parse the arguments, including an optional scale argument
    if (!PyArg_ParseTupleAndKeywords(args, keywds, "OO(ii)|fi", kwlist, &dst_surface_ptr, &src_surface_ptr, &x, &y, &scale, &alpha)) {
        return NULL;
    }

    SDL_Surface* dst_surface = PyCapsule_GetPointer(dst_surface_ptr, "SDL_Surface");
    SDL_Surface* src_surface = PyCapsule_GetPointer(src_surface_ptr, "SDL_Surface");

    if (!dst_surface || !src_surface) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid SDL_Surface pointer.");
        return NULL;
    }

    if ((scale != 1.0) || (alpha != 255)) {
        blit_and_scale_w_alpha(dst_surface, src_surface, x, y, scale, scale, alpha);
    } else {
        SDL_Rect dest_rect;
        dest_rect.x = x;
        dest_rect.y = y;
        SDL_BlitSurface(src_surface, NULL, dst_surface, &dest_rect);
    }

    Py_RETURN_NONE;
}



static PyObject* surface_size(PyObject* self, PyObject* args) {
    PyObject* surface_ptr;

    if (!PyArg_ParseTuple(args, "O", &surface_ptr)) {
        return NULL;
    }

    SDL_Surface* surface = PyCapsule_GetPointer(surface_ptr, "SDL_Surface");
    if (!surface) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid SDL_Surface pointer.");
        return NULL;
    }

    return Py_BuildValue("ii", surface->w, surface->h);
}


static PyObject* surface_bytes(PyObject* self, PyObject* args) {
    PyObject* surface_ptr;

    if (!PyArg_ParseTuple(args, "O", &surface_ptr)) {
        return NULL;
    }

    SDL_Surface* surface = PyCapsule_GetPointer(surface_ptr, "SDL_Surface");
    if (!surface) {
        PyErr_SetString(PyExc_RuntimeError, "Invalid SDL_Surface pointer.");
        return NULL;
    }

    // Create a bytes object from the surface's pixel data
    PyObject* bytes = PyBytes_FromStringAndSize((char*)surface->pixels, surface->pitch * surface->h);

    return bytes;
}


static PyObject* surface_new(PyObject* self, PyObject* args) {
    int width, height;

    // Parse the width and height from the Python arguments
    if (!PyArg_ParseTuple(args, "ii", &width, &height)) {
        return NULL;  // If parsing fails, return NULL to indicate an error.
    }

    // Define the pixel format for RGBA8888
    Uint32 rmask, gmask, bmask, amask;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    rmask = 0xff000000;
    gmask = 0x00ff0000;
    bmask = 0x0000ff00;
    amask = 0x000000ff;
#else
    rmask = 0x000000ff;
    gmask = 0x0000ff00;
    bmask = 0x00ff0000;
    amask = 0xff000000;
#endif

    // Create an SDL_Surface with the specified size and RGBA8888 format
    SDL_Surface* surface = SDL_CreateRGBSurface(0, width, height, 32, rmask, gmask, bmask, amask);
    if (surface == NULL) {
        PyErr_SetString(PyExc_RuntimeError, SDL_GetError());
        return NULL;  // Return NULL to indicate an error if surface creation fails.
    }

    // Return the surface as a PyCapsule
    return PyCapsule_New(surface, "SDL_Surface", free_sdl_surface);
}


// Function to load a PNG file as an SDL_Surface
static PyObject* surface_load(PyObject* self, PyObject* args) {
    const char* file_path;

    if (!PyArg_ParseTuple(args, "s", &file_path)) {
        return NULL;
    }

    SDL_Surface* surface = IMG_Load(file_path);
    if (!surface) {
        PyErr_SetString(PyExc_RuntimeError, IMG_GetError());
        return NULL;
    }

    return PyCapsule_New(surface, "SDL_Surface", free_sdl_surface);
}


static PyMethodDef softsdl2Methods[] = {
    {"screen_info", screen_info, METH_NOARGS, "Get the screen size and color depth"},
    {"window_create", (PyCFunction)window_create, METH_VARARGS | METH_KEYWORDS, "Create a new SDL window."},
    {"window_show", window_show, METH_VARARGS, "Show or hide a given SDL window."},
    {"window_close", window_close, METH_VARARGS, "Close the given SDL window."},
    {"window_surface", window_surface, METH_VARARGS, "Get the SDL_Surface associated with an SDL_Window"},
    {"window_update", window_update, METH_VARARGS, "Update the window surface"},
    {"window_fullscreen", window_fullscreen, METH_VARARGS, "Toggle fullscreen mode for a window."},
    {"window_info", window_info, METH_VARARGS, "Get information about a window."},
    {"quit", quit, METH_NOARGS, "Terminate all SDL systems."},
    {"event_enable_text_input", event_enable_text_input, METH_VARARGS, "Enable or disable text input mode."},
    {"event_post", event_post, METH_VARARGS, "Post an event for the next frame."},
    {"event_wait", event_wait, METH_VARARGS, "Return all event since the last frame; maintaining framerate or blocking until at least one event."},
    {"event_schedule", event_schedule, METH_VARARGS, "Create a new timer that emits an event at some rate for a given duration."},
    {"key_pressed_modifiers", key_pressed_modifiers, METH_NOARGS, "Returns a list of modifier keys currently held down."},
    {"mouse_pos", get_mouse_position, METH_NOARGS, "Get the current mouse position."},
    {"draw_clear", draw_clear, METH_VARARGS, "Clear the window with a given color."},
    {"draw_pixels", draw_pixels, METH_VARARGS, "Draw pixels from a numpy array onto an SDL window."},
    {"draw_lines", draw_lines, METH_VARARGS, "Draw unconnected lines from a numpy array onto an SDL window."},
    {"draw_polyline", draw_polyline, METH_VARARGS, "Draw a polyline using a series of points"},
    {"draw_rectangles", draw_rectangles, METH_VARARGS, "Draw filled or boarders of rectangles with a given width"},
    {"draw_triangles", draw_triangles, METH_VARARGS, "Draw unfilled or filled triangles"},
    {"draw_circles", draw_circles, METH_VARARGS, "Draw unfilled of filled circles"},
    {"draw_triangles_w_zbuffer", draw_triangles_w_zbuffer, METH_VARARGS, "Draw filled triangles with z-buffer, and optional index buffer"},
    {"draw", (PyCFunction)draw, METH_VARARGS | METH_KEYWORDS, "Generic draw"},
    {"draw_list_simplify", py_simplify_draw_list, METH_VARARGS, "Simplify a draw-list by removing covered shapes."},
    {"surface_new", surface_new, METH_VARARGS, "Create a new SDL_Surface of a given size with RGBA8888 format."},
    {"surface_subsurface", surface_subsurface, METH_VARARGS, "Create a subsurface from the main surface"},
    {"surface_blit", (PyCFunction)surface_blit, METH_VARARGS | METH_KEYWORDS, "Blit one surface onto another"},
    {"surface_size", surface_size, METH_VARARGS, "Return the (width, height) of the surface"},
    {"surface_load", surface_load, METH_VARARGS, "Load an image file as an SDL_Surface."},
    {"surface_bytes", surface_bytes, METH_VARARGS, "Return the pixel data of a surface."},
    {"font_load", font_load, METH_VARARGS, "Load a font for rendering text."},
    {"font_render", font_render, METH_VARARGS, "Draw text using a font to a new surface."},
    {"font_text_size", font_text_size, METH_VARARGS, "Get the size of rendered text"},
    {"font_info", font_info, METH_VARARGS, "Get information about the font"},
    {NULL, NULL, 0, NULL}
};

static struct PyModuleDef softsdl2module = {
    PyModuleDef_HEAD_INIT,
    "softsdl2",
    NULL,
    -1,
    softsdl2Methods
};

PyMODINIT_FUNC PyInit_softsdl2(void) {
    import_array();
    return PyModule_Create(&softsdl2module);
}
